# light-scattering-and-diagnostics

## Summary

This project aims to simulate various types of scattering spectra (Thomson, Raman, Rayleigh, ...) and the expected detected signal for several diagnostic setups (transmission grating spectrometer, ...).

## To start

* Install python. I recommend the [Anaconda python distribution](https://www.anaconda.com/download).
Some package might not be installed by default.
Please be sure you have installed the following:
    * brokenaxes: install it with pip for example:
    ```
    pip install brokenaxes
    ```
    * ...
* Setup access to gitlab.epfl.ch: log in once using tequila

* Ask permission to a gitlab admin to have access rights to the project

* Setup SSH keys etc as instructed by GitLab, then
```
git clone git@gitlab.epfl.ch:bvincent/light-scattering-and-diagnostics.git
```

* Create a dedicated branch for your contribution

* Ask for a merge request once you everything is ready

### To contribute

* Setup access to gitlab.epfl.ch: log in once using tequila

* Ask permission to a gitlab admin to add the access rights to the 
`spc/tcv/diag/ThomsonAnalysis` project

* Setup SSH keys etc as instructed by GitLab, then
```
git clone git@gitlab.epfl.ch:spc/tcv/diag/ThomsonAnalysis.git
```

* Create a dedicated branch for your contribution

* Ask for a merge request once you everything is ready and automated tests
are succesful

# Contact information:

* Benjamin Vincent: <mailto:benjamin.vincent@epfl.ch>
