# -*- coding: utf-8 -*-
"""
Created on Thu Feb  17 15:44:29 2022

@author: Benjamin Vincent

This script is a first draft that aim a simulting the case of an incoherent
Thomson scattering diagnsotic that would use 2 camera to estimate the electron
density. Such a method might work in situations when Rayleigh scattering 
contribution is much smaller. In these circustances one could use 
- one camera with a configuration that aim at collecting the maximum number of 
photon ( i.e the Thomson spectrum should mtach at best the spectral range where
the detection branhc is sensitive)
- one camera with a filter or scattering angle configuration (more or less 
sensitive to Doopler boradening) that would strongly modify the integrated 
signal measured from Thomson scattering when Te changes.
  
The design of the diagnostic would depend on the range of Te expected. 
- For cold palsma (below 500eV) it is probably better to have a filter in front 
of one a the cameras. Both cameras could be positionned at the same scattering 
angle but on each side of the equatorial plan (if the north pole is the 
polarization direction of the laser). In this case one should try to find the 
best fitler response shape that would give a linear increase of increase of the
detected signal over the largest temperature range (I guess it should be a 
quadratic spectral response.... but to be checkt).
- For hot plasma it is probably simpler to have one camera with a small 
scattering angle so that the spectral filter would be the spectral response of 
the camera. One could have both camera center on the scatterinmg angle at 
90 degree (one camera more senstive than the 90 configuration, the other less
sensitive).
"""
import sys
import time
import numpy               as np
import matplotlib.pyplot   as plt
import scipy.constants     as c
# import astropy.convolution as ap

sys.path.append('../') # add root of the project to the default paths

from brokenaxes                     import brokenaxes
from theory.Rayleigh                import Rayleigh_spectrum
from theory.Raman                   import Raman_spectrum
from theory.Thomson                 import Thomson_spectrum
from theory.distributions_functions import Gaussian
from data_fixed.get_fixed_data      import dtbs, trans#, trans_names

t_start = time.time()
dEdW    = {}
VBG     = {}
CCD     = {}
collect = {}
spectro = {}
laser   = {}
geom    = {}

np.seterr(divide='ignore', invalid='ignore')               # ignore the warning messsage from matplotlib in the function used to switch between rad/s and nm

###############################################################################
###############################################################################
############################### INITIALIZATIONS ###############################
###############################################################################
###############################################################################

###############################################################################
# Defintion and calculation of variables related to the transmission branch   #
###############################################################################

# Paramters of the laser used to generated the scattering signal
laser['lambda']  = 532e-9                                  # m       % laser wavelength
laser['omega']  = 2*c.pi*c.c/laser['lambda']               # rad s^-1% laser frequency
laser['energy'] = .4                                       # J       % laser energy
laser['tau']    = 1e-8                                     # s       % light pulse duration
laser['phi']    = 3e-4                                     # m       % laser beam diameter at the probed position

###############################################################################
# Defintion and calculation of variables related to the detection branch      #
###############################################################################

# Paramters of the CCD camera (iCCD usually)
CCD['height']          = 13e-3                                        # m       % full pixel height of the CCD sensor
CCD['width']           = CCD['height']                                # m       % full pixel height of the CCD sensor
CCD['H bin']           = 1                                            #         % number of pixel bins along the horizontal direction (perpendicular to the laser beam direction)
CCD['V bin']           = 1                                            #         % number of pixel bin along the vertical direction (along laser beam direction)
CCD['N pix']           = 1024                                         #         % number of pixel over one direction of the iCCD (hypothesis of square iCCD)
CCD['tau']             = 1e-8                                         # s       % duration of the amplification window of the ICCD

# Paramters of the collection lens configuration
collect['OA']          = -500e-3                                       # m       % distance of the collection lens from the laser beam    (value from Yanis for the AWAKE setup experiment)
collect['phi']         = 50e-3                                        # m       % diameter of the collection lens (value from Yanis for the AWAKE setup experiment)
collect['f']           = 50e-3                                        # m       % focal of the collection lens  (value from Yanis for the AWAKE setup experiment)
collect['OAp']         = 1/((1/collect['f'])+(1/collect['OA']))       # m       % image distance knowing the collection focal length and the image-lens distance (to be miniumized to maximize the solid angle of detection)
collect['gamma']       = collect['OAp']/collect['OA']                 #         % image magnification of the collection lens
collect['dOmega']      = 2*c.pi*(1-np.cos(np.arctan(.5*collect['phi']/collect['OA'])))# steradian       % solid angle of collection
collect['L']           = CCD['height']/abs(collect['gamma'])               # m       % length of the obervation volume
collect['d']           = CCD['width']*CCD['H bin']/(abs(collect['gamma'])*CCD['N pix'])# m       % width of one macro-pixel in the object plane (for the setup simulated in this script, it is better if it is bigger than the laser beam diameter)
collect['dtheta']      = np.arctan(collect['phi']/abs(collect['OA']))      # rad     % fluctuation of the scattering angle over the collection lens area 

# Spectrometer transmission function                                                       
Inst = lambda W: Gaussian(W, spectro['omega mean'], spectro['omega res'])     # NB: set the instrument function peak in the middle of the spectral interval to avoid shift induced by the convolution

# Oversampled omega array for precised calculation   
n_os                   = int(1e5)                                         # sampling coefficient between the 2 wavelength limits choosen
W_os,dW_os             = np.linspace(2*c.pi*c.c/432e-9,#432e-9,#350e-9,              # lowest wavelength limit bellow which quantum efficiency values for one of the ICCD amplifier are not available anymore
                                     2*c.pi*c.c/632e-9,#632e-9,#740e-9,              # highest wavelength limit above which quantum efficiency values for one of the ICCD amplifier are not available anymore
                                     n_os, retstep=True)    

###############################################################################
# Defintion of variables related to the scattering geometry                   #
###############################################################################

# Scattering configuration
geom['Ki']   = np.array([1, 0, 0])                         #         % direction of the wavevector of the incident wave
geom['Ks']   = np.array([0, 1, 0])                         #         % direction of the wavevector of the scattered wave
geom['Ei0']  = np.array([0, 0, 1])                         #         % direction of the electric field of the incident wave
geom['B0']   = np.array([1, 0, 0])                         #         % direction of the background magnetic field 
geom['Bm']   = 0                                         # T       % Magnitude of the magnetic field
geom['Ki']   = geom['Ki'] /np.linalg.norm(geom['Ki'])      #         % to be sure i is normed
geom['Ks']   = geom['Ks'] /np.linalg.norm(geom['Ks'])      #         % to be sure i is normed 
geom['Ei0']  = geom['Ei0']/np.linalg.norm(geom['Ei0'])     #         % to be sure i is normed
geom['B0']   = geom['B0'] /np.linalg.norm(geom['B0'])      #         % to be sure i is normed
geom['K']    = geom['Ks']-geom['Ki']                       #         % scattering wavevector 
geom['Kn']   = geom['K'] /np.linalg.norm(geom['K'])        #         % normed scattering wavevector 
geom['theta']= np.arccos(np.dot(geom['Ki'],geom['Ks']))    # rad     % scattering angle          
geom['phi']  = np.arccos(np.dot(geom['Ei0'],np.cross(geom['Ki'],geom['Ks'])))# rad     % angle between the normal to the scattering plane and the polarisation vector of the incident wave              
geom['psi']  = 0                                           # rad     % angle between polarisation of the incident wave and the one accepted by the detection branch
geom['beta'] = np.arccos(np.dot(geom['B0'],geom['Kn']))     # rad     % Angle between the magnetic field and the probed wavecector

print('''
#######################🔎 Light collection 🔎##################################
* Simulation with a {D_c:g} mm in diameter and a {f:g} m focal length collection lens
positionned {OA:g} mm from the laser beam image the scattering volume with a
{gamma:g} magnification
* With this configuration, the diagnostic probe a {L_probe:g} mm volume length with 
Ø {d_probe:g} mm, and photons are collected over {domega:g} sr'''
.format(D_c     = collect['phi']*1e3, OA      = collect['OA']*1e3, gamma  = collect['gamma'] , 
        L_probe = collect['L']*1e3  , d_probe = collect['d'] *1e3, domega = collect['dOmega'],f = collect['f']))
if collect['d']<laser['phi']: # if the size of a macro-pixel in the object plane is smaller than the laser beam diameter, it may be worth increasing the horizontal binning
     print('''* With this configuration the width of a macro-pixel on the object plane is
{pixel_width:g} mm while the beam diameter is {laser_phi:g} mm, it may be worth 
increasing the number of horizontal bin
'''.format(pixel_width=collect['d']*1e3 ,laser_phi=laser['phi']*1e3))
else:
    print('''* With this configuration the width of a macro-pixel on the object plane is
{pixel_width:g} mm while the beam diameter is {laser_phi:g} mm. 
'''.format(pixel_width=collect['d']*1e3 ,laser_phi=laser['phi']*1e3))
     
print('''
######################📐 Scattering geometry 📐################################
* Scattering angle of {theta:g} °
* Angle between scattering plane and EM wave polarisation is {phi:g} °'''
.format(theta = np.degrees(geom['theta']), phi = np.degrees(geom['phi'])), end='')
if isinstance(geom['psi'],(int,float)):
    print('''
* Only scattered light making an angle of {psi:g} ° with the EM wave polarisation 
is transmitted through the detection branch'''.format(psi=np.degrees(geom['psi'])))
else:
    print('''
* Both scattered light polarizations directions, are transmitted through the 
detection branch''')
           

###############################################################################
# Particles properties definition for Raman and Rayleigh scattering           #
###############################################################################
T0   = 300                        # K       % ambient temperature of the medium probe
P0   = 5                        # Pa      % ambient pressure of the medium probe

nitr = {'name' : 'N2'           , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => nothing implemented fo neutrals
        }

arg  = {'name' : 'Ar'           , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => only Maxwellian implemented fo neutrals
        }

argP = {'name' : 'Ar+'          , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => only Maxwellian implemented fo neutrals
        }

H2   = {'name' : 'H2'          , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => only Maxwellian implemented fo neutrals
        }

###############################################################################
# Particles properties definition for Thomson scattering                      #
###############################################################################
elec = {'name' : 'e-'           , #         % Name of scattering particle
        'T'    : 2*11605      ,   # K       % Temperature of scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % In case of a 100% degree of ionization 5e20           , # m^-3    % Density of of scattering particle
        'v'    : 0e5            , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration
         }

###############################################################################
# Temperature and ionization degree range choosen for the simulations         #
###############################################################################
T_min   = 1*11605                         # K       % Min temperature of scattering particle
T_max   = 1e2*11605                       # K       % Max temperature of scattering particle
T_r     = np.linspace(T_min,T_max,10)   # temperature of the scattering particles
alpha_i = np.linspace(0,1,10)          # ionization degrees investigated
                        
###############################################################################
###############################################################################
################################ CALCULATIONS #################################
###############################################################################
###############################################################################
scat_names = [] 
particles  = {'Rayleigh': [arg],
               # 'Raman'   : [H2],
               'Thomson' : [elec]
              }
spectrums  = {'Rayleigh': Rayleigh_spectrum,
              'Raman'   : Raman_spectrum   ,
              'Thomson' : Thomson_spectrum 
              }
detectors = ['HBf']  # ICCD detector intensifier generations. genIII-HBf, genIII-HRf, and genII-SR are available
    
for regime in particles:  # for all regime keys inside the particle dictionnary
    for i,particle in enumerate(particles[regime]):
        name = str(regime)+' '+str(particle['name'])+' '+str(particle['dist'])
        scat_names.append(name)
        
## Spectral density distribution of scattered photon energy spectrum
###############################################################################
        dEdW[name,'T low']   = spectrums[regime](W_os         , n_os   , laser        , geom  , collect, dtbs, particle,
                                                 particle['n'], T_r[1] , particle['v'], 'talk')
        dEdW[name,'T high']  = spectrums[regime](W_os    , n_os        , laser        , geom  , collect, dtbs, particle,
                                                 particle['n'], T_r[-1], particle['v'], 'talk')  
        
        dEdW[name] = lambda T: spectrums[regime](W_os         , n_os   , laser        , geom  , collect, dtbs, particle,
                                                 particle['n'], T      , particle['v'], '')
        
 # Impact of transmission and efficiency coefficient
###############################################################################
        dEdW[name,'trans max'] = lambda T: dEdW[name](T)  
        for gen in detectors:
            dEdW[name,'trans',gen] = lambda T: dEdW[name](T)*trans['quant eff ICCD '+gen,'func'](W_os)
        
## Number of photons per pixel along a bin row (vertical and horizontal binning taken into consideration)
###############################################################################
            dEdW[name,'photon number',gen] = np.array([sum(abs(dW_os)*dEdW[name,'trans',gen](i)/(c.hbar*W_os))
                                                       for i in T_r])/int(CCD['N pix']/CCD['V bin'])  
        
## Number of photons per pixel along a bin row depending on the ionization degrees (hypothesis of only one singly charge ion species)
###############################################################################       
            for i,elt in enumerate(T_r):
                if dtbs['q'][particle['name']]==0:
                    dEdW[name,'partial photon number', gen,elt] = dEdW[name,'photon number',gen][i]*(1-alpha_i)
                else:
                    dEdW[name,'partial photon number', gen, elt] = dEdW[name,'photon number',gen][i]*alpha_i


t_end = time.time()
print("⏳ The calculations took {:g} s, now let's plot.".format((t_end-t_start)))
###############################################################################
###############################################################################
#################################### PLOTS ####################################
###############################################################################
###############################################################################

## Scattering configuration
###############################################################################
fig = plt.figure(2)
rand_x = np.random.uniform(-1, 1, size=(20,)) # just some random x coordinates to plot plot the magnetic field like a stream
rand_y = np.random.uniform(-1, 1, size=(20,)) # just some random y coordinates to plot plot the magnetic field like a stream
rand_z = np.random.uniform(-1, 1, size=(20,)) # just some random z coordinates to plot plot the magnetic field like a stream
B_0n   = geom['B0']/2.5
ax = fig.add_subplot(projection='3d')
ax.quiver(-1.5         , 0            , 0            , 3             , 0             , 0             ,
          arrow_length_ratio=0   , color='C2', linewidth=.5)
ax.quiver(0            , 0            , 0            , geom['Ki'][0] , geom['Ki'][1] , geom['Ki'][2] ,
          arrow_length_ratio=0.25, color='C2', label='$\\vec{k}_{i}$')
ax.quiver(0            , 0            , 0            , geom['Ks'][0] , geom['Ks'][1] , geom['Ks'][2] ,
          arrow_length_ratio=0.25, color='C1', label='$\\vec{k}_{s}$')
ax.quiver(geom['Ki'][0], geom['Ki'][1], geom['Ks'][2], geom['K'][0]  , geom['K'][1]  , geom['K'][2]  ,
          arrow_length_ratio=0.25, color='C3', label='$\\vec{k}$')
ax.quiver(0            , 0            , 0            , geom['Ei0'][0], geom['Ei0'][1], geom['Ei0'][2],
          arrow_length_ratio=0.25, color='C0', label='$\\vec{E}_{i0}$')
for i in range(20):
    ax.quiver(rand_x[i], rand_y[i]    , rand_z[i]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
              arrow_length_ratio=0.5, color='C4')
ax.quiver(rand_x[0]    , rand_y[0]    , rand_z[0]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
          arrow_length_ratio=0.5, color='C4',label='$\\vec{B}_{0}$')
ax.grid(False) 
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
# ax.axis(False)
ax.set_xlim(-1.5, 1.5)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1) 
ax.legend()
plt.title('Scattering configuration \n $\\theta = {:g}$ ° and $\\phi = {:g}$ °'.format(np.degrees(geom['theta']) , np.degrees(geom['phi'])) )
# plt.savefig('Scattering_configuration_theta = %1.3f deg_phi = %1.3f deg.pdf'% (np.degrees(geom['theta']) , np.degrees(geom['phi'])))
plt.show()

## Spectral density distribution of scattered photon energy superposed to the ICCD quantum efficiencies
###############################################################################
fig, ax = plt.subplots()
ax.axvline(laser['omega'], linestyle='--', color='black', label='laser')

for elt in scat_names:
    ax.plot(W_os, dEdW[elt,'T low'] , label=elt+' at '+str("{:1.3g}".format(T_r[1]/11605)) +' eV')
    ax.plot(W_os, dEdW[elt,'T high'], label=elt+' at '+str("{:1.3g}".format(T_r[-1]/11605))+' eV')
ax.legend(loc='upper left')
# ax.set_xlim(W_os[0], W_os[-1])
ax.set_xlim(2e9*c.pi*c.c/500,2e9*c.pi*c.c/564)
ax.set_xlabel('pulsation /($rad.s^{-1}$)')
ax.set_ylabel('scattered energy distribution /($J.s.rad^{-1}$)')

axb = ax.twinx()
for gen in detectors:    
    axb.plot(W_os, trans['quant eff ICCD '+gen,'func'](W_os), label=gen+' ICCD', linestyle='--')
axb.set_ylabel('ICCD quantum efficiencies')
axb.set_ylim(0, .6)
axb.legend(loc='lower right')

secax = ax.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
# plt.tight_layout()
# plt.savefig('figure_1.pdf')
plt.show()

## Comparison of number of detected photon depending on the type of scattering and the temperature of the scattering particle
##############################################################################
fig, ax = plt.subplots()
for elt in scat_names:
    for gen in detectors:   
        ax.plot(T_r, dEdW[elt,'photon number',gen], label=elt+' with '+gen+' ICCD')
ax.legend()
ax.set_xlim(T_r[0], T_r[-1])
ax.set_xlabel('scattering particle temperature /($K$)')
ax.set_ylabel('#photons detected per macro-pixel')
plt.show()


## Relative variation of the number of photon expected on each macro-pixel depending on the scattering particle temperature
##############################################################################
fig, ax = plt.subplots()
for gen in detectors:
    ax.plot(T_r/11605, 100*abs(dEdW[elt,'photon number',gen]-dEdW[elt,'photon number',gen][0])/dEdW[elt,'photon number',gen][0],
            label='from Thomson with '+gen+' ICCD')
ax.legend()
ax.set_xlim(T_r[0]/11605, T_r[-1]/11605)
ax.set_xlabel('electron temperature /($eV$)')
ax.set_ylabel('change #photons detected per macro-pixel (%)')
# plt.tight_layout()
# plt.savefig('figure_2.pdf')
plt.show()

## Comparison of number of detected photon depending on the type of scattering and the temperature of the scattering particle
##############################################################################
fig, ax = plt.subplots()
axb = ax.twinx()
for gen in detectors:
    for i,elt in enumerate(scat_names[-1:]):
        ax.plot(alpha_i , dEdW[elt,'partial photon number',gen,T_r[0]], label=elt+' with '+gen+' ICCD at '+str(T_r[0]/11605)[0:4]+'eV')
    for i,elt in enumerate(scat_names[0:-1]):
        axb.plot(alpha_i, dEdW[elt,'partial photon number',gen,T_r[0]], label=elt+' with '+gen+' ICCD at '+str(T_r[0]/11605)[0:4]+'eV', linestyle='--')
ax.legend(loc='lower left')
ax.set_xlim(0, alpha_i[-1])
axb.legend(loc='upper right')
# ax.set_ylim(0, 1e3)
ax.set_xlabel('ionization degree')
axb.set_ylabel('#photons Rayleigh scattered detected \n per macro-pixel')
ax.set_ylabel('#photons Thomson scattered detected \n per macro-pixel')
# plt.tight_layout()
# plt.savefig('figure_3.pdf')
plt.show()

## Relative contribution from Rayleigh scattering (neutral + ion) with respect to Thomson scattering as a function of the ionization degree (for HBf CCD intensifier)
##############################################################################
fig, ax = plt.subplots()
for i,elt in enumerate(T_r):
    ax.plot(alpha_i, 100*dEdW['Rayleigh Ar Maxwellian','partial photon number','HBf',elt]/dEdW['Thomson e- Maxwellian','partial photon number','HBf',elt],
            label=str(elt/11605)[0:4]+'eV')
ax.legend()
ax.set_xlim(0, alpha_i[-1])
ax.set_xlabel('ionization degree')
ax.set_ylabel('Rayleigh scatterings/Thomson scattering (%)')
ax.set_title('with HBf ICCD')
# plt.savefig('figure_4.pdf')
plt.show()

# ## Relative contribution from Rayleigh scattering (neutral + ion) with respect to Thomson scattering as a function of the ionization degree (for HBf CCD intensifier)
# ##############################################################################
# fig, ax = plt.subplots()
# for i,elt in enumerate(T_r):
#     ax.plot(alpha_i, 100*(dEdW['Rayleigh Ar Maxwellian','partial photon number','SR',elt]+dEdW['Rayleigh Ar+ Maxwellian','partial photon number','SR',elt])/dEdW['Thomson e- Maxwellian','partial photon number','SR',elt],
#             label=str(elt/11605)[0:4]+'eV')
# ax.legend()
# ax.set_xlim(0, alpha_i[-1])
# ax.set_xlabel('ionization degree')
# ax.set_ylabel('Rayleigh scatterings/Thomson scattering (%)')
# ax.set_title('with SR ICCD')
# # plt.tight_layout()
# # plt.savefig('figure_4.pdf')
# plt.show()

## Variation of the total scattering photons as a function of the ionization degree
##############################################################################
# fig, ax = plt.subplots()
# ax.plot(alpha_i, dEdW['Rayleigh Ar Maxwellian','partial photon number','HBf',T_r[0]]+dEdW['Rayleigh Ar+ Maxwellian','partial photon number','HBf',T_r[0]]+dEdW['Thomson e- Maxwellian','partial photon number','HBf',T_r[0]],
#             label='scattering particles at '+str(T_r[0]/11605)[0:4]+'eV')
# ax.legend()
# ax.set_xlim(0, alpha_i[-1])
# ax.set_xlabel('ionization degree')
# ax.set_ylabel('#photons scattered by Rayleigh and Thomson \n per macro-pixel')
# ax.set_title('with SR ICCD')
# # plt.tight_layout()
# # plt.savefig('figure_5.pdf')
# plt.show()
