# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 11:44:01 2021

@author: Benjamin Vincent

This script is used to estimate the expected number of photons detected from 
Rayleigh, Raman and Thomson scattering with a transmission grating spectrometer 
coupled to an intensified ICCD.
The calculation is perfomed depending on the specification inputs relative to
- the laser
- the ICCD
- the spectromer that include the specification on
    - the central wavelength and aperture of the spectrometer
    - the VBG notch filter (transmission curve loaded from the data folder)
    - the diffraction grating (difffraction efficiency curves loaded from the data folder if this mode is selected see collect['trans_coeff'])
- scattering configuration
- light collection configuration

Few plots are also perfomed to compare transmission efficiency of this 
hypothetical diagnostic to the currently used polychromator diagnostics used 
for Thomson scattaring investigations inside tokamak.
"""
import sys
import time
import numpy               as np
import matplotlib.pyplot   as plt
import scipy.constants     as c
import astropy.convolution as ap

sys.path.append('../') # add root of the project to the default paths

from brokenaxes                     import brokenaxes
from theory.Rayleigh                import Rayleigh_spectrum
from theory.Raman                   import Raman_spectrum
from theory.Thomson                 import Thomson_spectrum
from theory.distributions_functions import Gaussian
from data_fixed.get_fixed_data      import dtbs, trans, trans_names

t_start = time.time()
dEdW    = {}
VBG     = {}
CCD     = {}
collect = {}
spectro = {}
laser   = {}
geom    = {}

np.seterr(divide='ignore', invalid='ignore')               # ignore the warning messsage from matplotlib in the function used to switch between rad/s and nm

###############################################################################
###############################################################################
############################### INITIALIZATIONS ###############################
###############################################################################
###############################################################################

###############################################################################
# Defintion and calculation of variables related to the transmission branch   #
###############################################################################

# Paramters of the laser used to generated the scattering signal
laser['lambda'] = 532e-9                                  # m       % laser wavelength
laser['omega']  = 2*c.pi*c.c/laser['lambda']               # rad s^-1% laser frequency
laser['energy'] = 1                                        # J       % laser energy
laser['tau']    = 10e-9                                    # s       % light pulse duration
laser['phi']    = 1e-3                                     # m       % laser beam diameter at the probed position

###############################################################################
# Defintion and calculation of variables related to the detection branch      #
###############################################################################

# Paramters of the CCD camera (iCCD usually)
CCD['height']          = 13e-3                                        # m       % full pixel height of the CCD sensor
CCD['width']           = CCD['height']                                # m       % full pixel height of the CCD sensor
CCD['H bin']           = 32                                            #         % number of pixel bins along the horizontal direction (spectral)
CCD['V bin']           = 1024                                            #         % number of pixel bin along the vertical direction (along laser beam direction)
CCD['N pix']           = 1024                                         #         % number of pixel over one direction of the iCCD (hypothesis of square iCCD)

# Paramters of the Volume Bragg Grating filter
VBG['dtheta']          = np.radians(1.2)                              # rad     % acceptance angle of the volume Bragg grating
VBG['width']           = 50e-3                                        # m       % width of the VGB notch filter (usually a square)

# Paramters of the spectrometer design (transmission spectro)
spectro['f']           = 0.3                                          # m       % spectrometer collimation focal lens choosen
spectro['grating']     = 4000e3                                       # l m^-1  % line frequency of the grating used inside the spectrometer
spectro['slit']        = 150e-6                                       # m       % input slit width
spectro['angle']       = np.radians(20)                               # rad     % diffraction angle of the grating used by the spectrometer
spectro['f min']       = .5*CCD['height']/np.tan(VBG['dtheta'])       # m       % minimal spectrometer collimation focal length for efficient VBG filtering
spectro['phi min']     = np.sqrt(2*VBG['width']**2)                   # m       % minimal diameter of the lenses inside the transmission spectrometer
spectro['lambda mean'] = 532e-9                                       # m       % spectrometer mean central wavelength
spectro['Dlambda']     = CCD['width']*np.cos(spectro['angle'])/(spectro['f']*spectro['grating'])# m       % spectrometer wavelength coverage
spectro['lambda min']  = spectro['lambda mean']-spectro['Dlambda']/2  # m       % left wavelength limit on the iCDD
spectro['lambda max']  = spectro['lambda mean']+spectro['Dlambda']/2  # m       % right wavelength limit on the iCDD
spectro['lambda res']  = np.cos(spectro['angle'])*spectro['slit']/(spectro['grating']*spectro['f'])# m       % spectral width of the instrumental function
spectro['LAMBDA'] ,\
spectro['dLAMBDA']     = np.linspace(spectro['lambda min'],           # m       % wavelengths probed by each pixel and spectral step between each pixel
                                     spectro['lambda max'],
                                     int(CCD['N pix']/CCD['H bin']),
                                     retstep=True)
spectro['omega mean']  = 2*c.pi*c.c/spectro['lambda mean']
spectro['OMEGA']       = 2*c.pi*c.c/spectro['LAMBDA']                 # rad s^-1% pulsation probed by each pixel
spectro['dOMEGA']      = 2*c.pi*c.c*spectro['dLAMBDA']/(spectro['LAMBDA']**2)
spectro['omega res']   = 2*c.pi*c.c*spectro['lambda res']/laser['lambda']**2
spectro['pix res']     = spectro['lambda res']*CCD['N pix']/spectro['Dlambda']

# Paramters of the collection lens configuration
collect['OA']          = 100e-3                                       # m       % distance of the collection lens from the laser beam   
collect['phi']         = 45e-3                                        # m       % diameter of the collection lens 
collect['OAp']         = spectro['f']*collect['phi']/VBG['width']     # m       % image distance that give a configuration with the spectro accptance angle completly filled
collect['gamma']       = collect['OAp']/collect['OA']                 #         % image magnification of the collection lens
collect['dOmega']      = 2*c.pi*(1-np.cos(np.arctan(.5*collect['phi']/collect['OA'])))# steradian       % solid angle of collection
collect['L']           = CCD['height']/collect['gamma']               # m       % length of the obervation volume
collect['d']           = spectro['slit']/collect['gamma']             # m       % diameter of the obervation volume (if the slit width match the laser beam size)
collect['dtheta']      = np.arctan(collect['phi']/collect['OA'])      # rad     % fluctuation of the scattering angle over the collection lens area more all less equal to geom['dtheta_s'] the fluctuations of the scattering angle with the magnetic field direction 
collect['trans_coeff'] = False                                        # boolen  % to activate or not the use of the transmission curves selected inside the database script


# Spectrometer transmission function                                                       
Inst = lambda W: Gaussian(W, spectro['omega mean'], spectro['omega res'])     # NB: set the instrument function peak in the middle of the spectral interval to avoid shift induced by the convolution

# Oversampled omega array for precised calculation   
n_os                   = 250                                           # oversampling coefficient to avoid missing the sharp Raman and Rayleigh peaks 
W_os,dW_os             = np.linspace(spectro['OMEGA'][-1],           # Artifical over sampled equispaced pulsation range to do not miss the peak from Raman scattering and simplify integration
                                     spectro['OMEGA'][0],
                                     CCD['N pix']*n_os,
                                     retstep=True)    

###############################################################################
# Defintion of variables related to the scattering geometry                   #
###############################################################################

# Scattering configuration
geom['Ki']      = np.array([1, 0, 0])                         #         % direction of the wavevector of the incident wave
geom['Ks']      = np.array([0, 1, 0])                         #         % direction of the wavevector of the scattered wave
geom['Ei0']     = np.array([0, 0, 1])                         #         % direction of the electric field of the incident wave
geom['B0']      = np.array([1, 0, 0])                         #         % direction of the background magnetic field 
geom['Bm']      = 0                                           # T       % Magnitude of the magnetic field
geom['Ki']      = geom['Ki'] /np.linalg.norm(geom['Ki'])      #         % to be sure i is normed
geom['Ks']      = geom['Ks'] /np.linalg.norm(geom['Ks'])      #         % to be sure i is normed 
geom['Ei0']     = geom['Ei0']/np.linalg.norm(geom['Ei0'])     #         % to be sure i is normed
geom['B0']      = geom['B0'] /np.linalg.norm(geom['B0'])      #         % to be sure i is normed
geom['K']       = geom['Ks']-geom['Ki']                       #         % scattering wavevector 
geom['Kn']      = geom['K'] /np.linalg.norm(geom['K'])        #         % normed scattering wavevector 
geom['theta']   = np.arccos(np.dot(geom['Ki'],geom['Ks']))    # rad     % scattering angle      
geom['phi']     = np.arccos(np.dot(geom['Ei0'],np.cross(geom['Ki'],geom['Ks'])))# rad     % angle between the normal to the scattering plane and the polarisation vector of the incident wave              
geom['psi']     = 0                                           # rad     % angle between polarisation of the incident wave and the one accepted by the detection branch
geom['beta']    = np.arccos(np.dot(geom['B0'],geom['Kn']))    # rad     % Angle between the magnetic field and the probed wavecector
# geom['beta']    = .99999*c.pi/2                               # rad     % manual entry, usually to force the probe wavevector to be perfectly perpendicular to the magnetic field direction
geom['theta_s'] = np.arccos(np.dot(geom['B0'],geom['Ks']))    # rad     % Angle between the magnetic field and the probed wavecector
# geom['ksii']    = np.arccos(np.dot(geom['B0'],geom['Ki']))    # rad     % angle between the background magnetic field and the incident wavevector
# geom['ksis']    = np.arccos(np.dot(geom['B0'],geom['Ks']))    # rad     % angle between the background magnetic field and the scattered wavevector

print('''
####################🌈 Inside the spectrometer 🌈##############################
* VBG notch filter size and acceptance angle are {VBG_size} mm and {acceptance:g} degrees
* Efficient filterring over the {slit_h} mm slit height if collimation length > {fs_min:g} mm
* In pratice, f = {fs:g} mm, and incircle of the VBG square surface considered as 
the limiting size (diameter of {W_VBG:g} mm)
* This optic configuration lead to a f/{aperture:g} spectrometer aperture
* With the {grating:g} l/mm grating, a {Dlamda:g} nm spectral coverage is obtained, 
a slit width of {slit_w:g} mm gives a {resol:g} nm resolution (equivalent to {pix_res:g} pixels)

#######################🔎 Light collection 🔎##################################
* To match the spectrometer aperture, the {D_c:g} mm in diameter collection lens  
has to be {OAp:g} mm from the spectrometer input slit, it gives a {gamma:g} 
magnification
* With this configuration, the diagnostic probe a {L_probe:g} mm volume length with 
Ø {d_probe:g} mm, and photons are collected over {domega:g} sr

######################📐 Scattering geometry 📐################################
* Scattering angle of {theta:g} °
* Angle between scattering plane and EM wave polarisation is {phi:g} °'''
.format(VBG_size = VBG['width']*1e3         , acceptance = np.degrees(VBG['dtheta']),
        slit_h   = CCD['height']*1e3        , fs_min     = spectro['f min']*1e3,
        fs       = spectro['f']*1e3         , W_VBG      = VBG['width']*1e3,
        aperture = spectro['f']/VBG['width'], D_c        = collect['phi']*1e3,
        OAp      = collect['OAp']*1e3       , gamma      = collect['gamma'],
        grating  = spectro['grating']/1e3   , Dlamda     = spectro['Dlambda']*1e9,
        L_probe  = collect['L']*1e3         , d_probe    = collect['d']*1e3       , domega  = collect['dOmega'],
        theta    = np.degrees(geom['theta']), phi        = np.degrees(geom['phi']),
        slit_w   = spectro['slit']*1e3      ,resol=spectro['lambda res'] *1e9     , pix_res = spectro['pix res']),end='')
if isinstance(geom['psi'],(int,float)):
    print('''
* Only scattered light making an angle of {psi:g} ° with the EM wave polarisation 
is transmitted through the detection branch'''.format(psi = np.degrees(geom['psi'])))
else:
    print('''
* Both scattered light polarizations directions, are transmitted through the 
detection branch''')
      

###############################################################################
# Particles properties definition for Raman and Rayleigh scattering           #
###############################################################################
T0   = 300                        # K       % ambient temperature of the medium probe
P0   = 100                        # Pa      % ambient pressure of the medium probe

arg  = {'name' : 'Ar'           , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => only Maxwellian implemented fo neutrals
        }


nitr = {'name' : 'N2'           , #         % Name of scattering particle
        'T'    : T0             , # K       % Temperature of the scattering particle
        'T_rov': T0             , # K       % Ro-vibrationnal states temperature of the scattering particle
        'P'    : P0             , # Pa      % partial pressure of the scattering particle
        'n'    : P0/(c.k*T0)    , # m^-3    % Density of of scattering particle
        'n_rov': P0/(c.k*T0)    , # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration => only Maxwellian implemented fo neutrals
        }

###############################################################################
# Particles properties definition for Thomson scattering                      #
###############################################################################
elec = {'name' : 'e-'           , #         % Name of scattering particle
        'T'    : 100*11605      , # K       % Temperature of scattering particle
        'n'    : 5e20           , # m^-3    % Density of of scattering particle
        'v'    : 0e5            , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'   #         % Type of scattering particle behavior for velocity distribution consideration
         }

elecB = {'name' : 'e-'           , #         % Name of scattering particle
        'T'    : 100*11605      , # K       % Temperature of scattering particle
        'n'    : 5e20           , # m^-3    % Density of of scattering particle
        'v'    : 0e5            , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'magnetized'   #         % Type of scattering particle behavior for velocity distribution consideration
         }

prot = {'name' : 'p+'           , #         % Name of scattering particle
        'T'    : elec['T']      , # K       % Temperature of scattering particle
        'n'    : elec['n']      , # m^-3    % Density of of scattering particle
        'v'    : 0              , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'     #         % Type of scattering particle behavior for velocity distribution consideration
        }
                                          
###############################################################################
###############################################################################
################################ CALCULATIONS #################################
###############################################################################
###############################################################################
scat_names = [] 
particles  = {
              # 'Rayleigh': [nitr],
              'Raman'   : [nitr],
              # 'Thomson' : [elecB]
              }
spectrums  = {'Rayleigh': Rayleigh_spectrum,
              'Raman'   : Raman_spectrum   ,
              'Thomson' : Thomson_spectrum 
              }
    
for regime in particles:  # for all regime keys inside the particle dictionnary
    for j,particle in enumerate(particles[regime]):
        name = str(regime)+' '+str(particle['name'])+' '+str(particle['dist'])
        scat_names.append(name)
        
## Spectral density distribution of scattered photon energy spectrum
###############################################################################
        dEdW[name]                 = spectrums[regime](W_os         , n_os         , laser        , geom  , collect, dtbs, particle,
                                                       particle['n'], particle['T'], particle['v'], 'talk')
        dEdW[name,'dN']            = spectrums[regime].dN_s

# Impact of transmission and efficiency coefficient
###############################################################################
        if collect['trans_coeff']:
            dEdW[name,'trans'] = dEdW[name]*trans['trans grating','func'](W_os)# do not consider the impact of transmissions efficiency of optic and filters
        else:
            dEdW[name,'trans'] = dEdW[name]                                    # do consider the impact of transmissions efficiency of optic and filters
     
# Convolution instrument profile
###############################################################################
        dEdW[name,'trans','convo'] = ap.convolve(dEdW[name,'trans'],ap.Gaussian1DKernel(int(spectro['pix res']*n_os)))

## Number of photons per pixel along a bin row (vertical and horizontal binning taken into consideration)
###############################################################################
        dEdW[name,'photon number'] = np.array([sum(dW_os*dEdW[name,'trans','convo'][(W_os>spectro['OMEGA'][i]-spectro['dOMEGA'][i]/2) & (W_os<spectro['OMEGA'][i]+spectro['dOMEGA'][i]/2)])\
                                              /(c.hbar*spectro['OMEGA'][i])
                                              for i in range(int(CCD['N pix']/CCD['H bin']))])/int(CCD['N pix']/CCD['V bin'])  

t_end = time.time()
print("⏳ The calculations took {:g} s, now let's plot.".format((t_end-t_start)))

###############################################################################
###############################################################################
#################################### PLOTS ####################################
###############################################################################
###############################################################################

## Collection lens config
###############################################################################
magni   = np.linspace(0.1,1.25,1000)# range of possible magnifiction
diam_c  = VBG['width']*magni*collect['OA']/spectro['f']# range range of optimal collection lens diameters
dOmegas = 2*c.pi*(1-np.cos(np.arctan(diam_c/(2*collect['OA']))))
L_beam  = CCD['height']/magni

fig0 = plt.figure()
gs = fig0.add_gridspec(3, hspace=0)
ax0 = gs.subplots(sharex=True)
ax0[0].plot(magni,dOmegas)
ax0[0].axvline(collect['gamma'],color='C9',linestyle='--')
ax0[0].set_ylabel(r'$\Delta \Omega$ /(sr)')
ax0[1].plot(magni,L_beam)
ax0[1].axvline(collect['gamma'],color='C9',linestyle='--')
ax0[1].set_ylabel(r'$L_{laser}$ /(m)')
ax0[2].plot(magni,dOmegas*L_beam)
ax0[2].axvline(collect['gamma'],color='C9',linestyle='--')
ax0[2].set_xlabel('collection lens magnification')
ax0[2].set_xlim(magni[0],magni[-1])
ax0[2].set_ylabel(r'$\Delta \Omega * L_{laser}$  /(m sr)')
secax = ax0[0].secondary_xaxis('top', functions=(lambda x: x*VBG['width']*collect['OA']/spectro['f'],
                                                  lambda x: x*VBG['width']*collect['OA']/spectro['f']))
secax.set_xlabel('collection lens diameter /(m)')
plt.show()


## Scattering configuration
###############################################################################
fig = plt.figure(2)
rand_x = np.random.uniform(-1, 1, size=(20,)) # just some random x coordinates to plot plot the magnetic field like a stream
rand_y = np.random.uniform(-1, 1, size=(20,)) # just some random y coordinates to plot plot the magnetic field like a stream
rand_z = np.random.uniform(-1, 1, size=(20,)) # just some random z coordinates to plot plot the magnetic field like a stream
B_0n   = geom['B0']/2.5
ax = fig.add_subplot(projection='3d')
ax.quiver(-1.5         , 0            , 0            , 3             , 0             , 0             ,
          arrow_length_ratio=0   , color='C2', linewidth=.5)
ax.quiver(0            , 0            , 0            , geom['Ki'][0] , geom['Ki'][1] , geom['Ki'][2] ,
          arrow_length_ratio=0.25, color='C2', label='$\\vec{k}_{i}$')
ax.quiver(0            , 0            , 0            , geom['Ks'][0] , geom['Ks'][1] , geom['Ks'][2] ,
          arrow_length_ratio=0.25, color='C1', label='$\\vec{k}_{s}$')
ax.quiver(geom['Ki'][0], geom['Ki'][1], geom['Ks'][2], geom['K'][0]  , geom['K'][1]  , geom['K'][2]  ,
          arrow_length_ratio=0.25, color='C3', label='$\\vec{k}$')
ax.quiver(0            , 0            , 0            , geom['Ei0'][0], geom['Ei0'][1], geom['Ei0'][2],
          arrow_length_ratio=0.25, color='C0', label='$\\vec{E}_{i0}$')
for i in range(20):
    ax.quiver(rand_x[i], rand_y[i]    , rand_z[i]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
              arrow_length_ratio=0.5, color='C4')
ax.quiver(rand_x[0]    , rand_y[0]    , rand_z[0]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
          arrow_length_ratio=0.5, color='C4',label='$\\vec{B}_{0}$')
ax.grid(False) 
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
ax.set_xlim(-1.5, 1.5)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1) 
ax.legend()
plt.title('Scattering configuration \n $\\theta = {:g}$ ° and $\\phi = {:g}$ °'.format(np.degrees(geom['theta']) , np.degrees(geom['phi'])) )
# plt.savefig('Scattering_configuration_theta = %1.3f deg_phi = %1.3f deg.pdf'% (np.degrees(geom['theta']) , np.degrees(geom['phi'])))
plt.show()


## Spectral density distribution of scattered photon energy
###############################################################################
fig1, ax1 = plt.subplots()
ax1.axvline(laser['omega'],linestyle='--',color='C9',label='laser')
for i,elt in enumerate(scat_names):
    ax1.plot(W_os,dEdW[elt],label=elt)
ax1.legend()
ax1.set_xlim(W_os[0], W_os[-1])
ax1.set_xlabel('pulsation /($rad.s^{-1}$)')
ax1.set_ylabel('scattered energy distribution /($J.s.rad^{-1}$)')
secax = ax1.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
plt.show()


# Spectral density distribution of scattered photon energy transmitted
##############################################################################
fig2, ax2 = plt.subplots()
ax2.axvline(laser['omega'],linestyle='--',color='C9',label='laser')
for i,elt in enumerate(scat_names):
    ax2.plot(W_os,dEdW[elt,'trans'],label=elt)
ax2.legend()
ax2.set_xlim(W_os[0], W_os[-1])
ax2.set_xlabel('pulsation /($rad.s^{-1}$)')
ax2.set_ylabel('scattered energy distribution /($J.s.rad^{-1}$)')
secax = ax2.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
plt.show()


## Spectral density distribution of scattered photon energy transmitted and convoluted
###############################################################################
fig3, ax3 = plt.subplots()
ax3.axvline(laser['omega'],linestyle='--',color='C9',label='laser')
for i,elt in enumerate(scat_names):
    ax3.plot(W_os,dEdW[elt,'trans','convo'],label=elt)
ax3.legend()
ax3.set_xlim(W_os[0], W_os[-1])
ax3.set_xlabel('pulsation /($rad.s^{-1}$)')
ax3.set_ylabel('scattered energy distribution /($J.s.rad^{-1}$)')
secax = ax3.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
plt.show()


## Number of photons per pixel
###############################################################################
fig4, ax4 = plt.subplots(constrained_layout=True)
ax4.axvline(laser['omega'],linestyle='--',color='C9',label='laser')
for i,elt in enumerate(scat_names):
    ax4.bar(spectro['OMEGA'], dEdW[elt,'photon number'], spectro['dOMEGA'],
            yerr  = dEdW[elt,'photon number']**(1/2),
            label = elt+' ('+str("{:1.3g}".format(dEdW[elt,'dN']))+' photons collected)' , edgecolor ='black', alpha=.2)
ax4.legend()
ax4.set_xlabel('pulsation /($rad.s^{-1}$)')
ax4.set_ylabel('number of photons detected per macro-pixel')
ax4.set_xlim(W_os[0], W_os[-1])
secax = ax4.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
ax4.legend()
# plt.savefig('number_of_photon_5mbar_N2.pdf')
plt.show()

if collect['trans_coeff']:
    ## Spectral profile of photon transmission coefficients
    ###############################################################################
    bax1 = brokenaxes(xlims=((1.76e15, 1.825e15),(3.475e15, 3.608e15)))
    for i,elt in enumerate(trans_names):
        bax1.plot(trans['W broad'],trans[elt,'func'](trans['W broad']),label=elt)
    bax1.axvline(2e9*c.pi*c.c/532,linestyle='--',color='green')
    bax1.axvline(2e9*c.pi*c.c/1064,linestyle='--',color='red')
    bax1.legend()
    bax1.set_ylim(0 , 1)
    bax1.set_xlabel('wavelength /($nm$)')
    bax1.set_ylabel('transmission coefficient')
    secax = bax1.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
    plt.show()
    
    
    ## Spectral profile of photon transmission coefficients
    ###############################################################################
    bax2 = brokenaxes(xlims=((1.76e15, 1.825e15),(3.475e15, 3.608e15)))
    bax2.plot(trans['W broad'],
              trans['trans grating','func'](trans['W broad']),
              label='transmission spectrometer design')
    bax2.plot(trans['W broad'],
              trans['polychromator','func'](trans['W broad']),
              label='polychromtor design')
    bax2.axvline(2e9*c.pi*c.c/532,linestyle='--',color='green')
    bax2.axvline(2e9*c.pi*c.c/1064,linestyle='--',color='red')
    bax2.legend()
    bax2.set_ylim(0, .5)
    bax2.set_xlabel('wavelength /($nm$)')
    bax2.set_ylabel('transmission coefficient')
    secax = bax2.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
    # plt.savefig('transmission_coeff.pdf')
    plt.show()
