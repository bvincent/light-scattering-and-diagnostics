# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 17:10:39 2021

@author: Administrator

From Yanis measurements the angle would be ~=14 degrees => theta_s~=76 degrees
"""

import numpy              as np
import matplotlib.pyplot  as plt
import scipy.constants    as c

# Doppler effet due to a pure Doppler shift (no distribution taken into account)
omega_i = 2*c.pi*c.c/1064e-9
the_i   = np.radians(90)
the_s   = np.radians(76)
doopler = lambda x,beta: (1-beta*np.cos(the_i))/(1-beta*np.cos(x))

beta = 1  # v/c
v_el = np.array([0, 1])                               # runaway beam direction
K_i  = np.array([1, 0])                               #         % direction of the wavevector of the incident wave
K_s  = np.array([np.cos(the_i-the_s), 
                 np.sin(the_i-the_s)])*doopler(the_s,beta) #         % direction of the wavevector of the scattered wave
K     = K_i-K_s
Kn    = K    / np.linalg.norm(K)                      #         % to be sure i is normed
v_el  = v_el / np.linalg.norm(v_el)
the_v = np.arccos(np.dot(Kn,v_el))                    # angle between runaway beam and probe wave-vector


## Scattering configuration with change in ks length taken into account
###############################################################################
figd1, axd1 = plt.subplots()

axd1.arrow(0     , 0        , K_i[0] , K_i[1]   , color='C2',label='$\\vec{k}_{i}$')
axd1.arrow(0     , 0        , K_s[0] , K_s[1]    , color='C1',label='$\\vec{k}_{s}$')
axd1.arrow(0     , 0        , v_el[0], v_el[1]    , color='C4',label='$\\vec{v}_{e}$')
axd1.arrow(K_s[0], K_s[1],  K[0]   , K[1]   , color='C3',label='$\\vec{k}$')
axd1.set_xlim(-.1, 1.3)
axd1.set_ylim(-.1, 1.3)
axd1.grid(False)
axd1.legend()
plt.title('Scattering configuration \n $\\theta = {:g}$ ° and $\\theta_v = {:g}$ °'.format(np.degrees(the_i-the_s),np.degrees(the_v)) )
plt.show()

## Change in scattered wavelength and angle between runaway beam and probe wave-vector at a given beta
###############################################################################

figd2a, axd2a = plt.subplots()

the   = np.radians(np.linspace(-95,95,1000))
the_vall = np.arctan(-(1-np.cos(the_i-the)*doopler(the,beta))/(np.sin(the_i-the)*doopler(the,beta)))

axd2a.plot(np.degrees(the),2e9*c.pi*c.c/(omega_i*doopler(the,beta)),color='C0')
axd2a.set_ylim(500 , 1100)
axd2a.set_xlabel('angle between $\\vec{k}_{s}$ and $\\vec{v}_{e}$ /(°)')
axd2a.set_ylabel('scattered wavelength by e- at $\\beta=$ {:g}'.format(beta))
axd2a.tick_params(axis='y', labelcolor='C0')

axd2b = axd2a.twinx()
axd2b.plot(np.degrees(the),np.degrees(the_vall),color='C1')
axd2b.set_ylabel('angle between $\\vec{k}$ and $\\vec{v}_{e}$ /(°)')
axd2b.tick_params(axis='y', labelcolor='C1')

plt.show()


## 
###############################################################################


figd3, axd3 = plt.subplots()

betas = np.linspace(.2,1,100)

axd3.plot(betas,2e9*c.pi*c.c/(omega_i*doopler(the_s,betas)),color='C0')
# axd3.set_ylim(500 , 1100)
axd3.set_ylabel('$\\beta$ $ ')
axd3.set_ylabel('scattered wavelength by e- at $\\theta_s=$ {:g} °'.format(np.degrees(the_s)))
axd3.tick_params(axis='y', labelcolor='C0')

# axd2b = axd2a.twinx()
# axd2b.plot()
# axd2b.set_ylabel('angle between $\\vec{k}$ and $\\vec{v}_{e}$ /(°)')
# axd2b.tick_params(axis='y', labelcolor='C1')

plt.show()