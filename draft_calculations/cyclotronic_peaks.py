# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 14:52:18 2021

@author: Benjamin Vincent

List of references:
    BOB_xxxx : http://xxx
"""

import scipy.constants   as c
import numpy             as np
import matplotlib.pyplot as plt

# Define parameters: incident laser and magnetic field inside the plasma
B        = 1.5               # T
omega_c  = abs(c.e*B/c.m_e)    # rad.s**-1
lambda_0 = 532e-9                # m
omega_0  = 2*c.pi*c.c/lambda_0 # rad.s**-1

# Calculate position of cyclotronic peaks
N_peak      = np.arange(-1e5,1e5,1)
omega_peak  = omega_0+omega_c*N_peak
lambda_peak = 2*c.pi*c.c/omega_peak
peak        = lambda_peak/lambda_peak

# Calculate distance between adjacent peaks
dLambda     = 2*c.pi*omega_c/(omega_0**2-(2*N_peak+1)*omega_0*omega_c+N_peak*(N_peak+1)*omega_c**2)

# Plot
fig, ax = plt.subplots()
ax.plot(lambda_peak*1e9,peak,marker='+',linestyle='None')
secax   = ax.secondary_xaxis('top', functions=(lambda x: 2*c.pi*c.c/x,
                                               lambda x: 2*c.pi*c.c/x))
ax.set_xlabel('wavelength /(nm)')
secax.set_xlabel('pulsation /(rad.s$^{-1}$)')
ax.set_xlim(531.5,532)
plt.show()
