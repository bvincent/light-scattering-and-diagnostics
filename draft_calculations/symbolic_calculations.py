# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 16:21:37 2021

@author: Benjamin Vincent

To try: try to integrate with beta and impose olimit to beta value
"""
import sympy as sy

a    = sy.symbols('a',real=True,positive=True)
beta    = sy.symbols('v',real=True,positive=True)
c    = sy.symbols('c',real=True,positive=True)
v    = sy.symbols('v',real=True,positive=True)

###############################################################################
# Non relativistic                                                            #
###############################################################################

fv = sy.exp(-v**2/a)

primi = sy.integrate(fv*v**2,v)


## Relativistic case 



###############################################################################
# Relativistic                                                                #
###############################################################################

# func_i = beta*(1/sy.sqrt(1-beta**2))**2*sy.exp(-a/sy.sqrt(1-beta**2)) # without aby approximation

# func = beta*(1+beta**2)*sy.exp(-a*(1+.5*beta**2)) # first order DL in v/c

# primi = sy.integrate(func,beta)

print(primi)