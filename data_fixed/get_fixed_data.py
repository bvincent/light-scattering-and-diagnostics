# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 14:16:21 2021


@author: Benjamin Vincent

List of references:
    NIST_CCC-BDB      : https://cccbdb.nist.gov/exp1x.asp
    Penney_1974       : https://doi.org/10.1364/JOSA.64.000712
    vandeSande_2002   : https://pure.tue.nl/ws/files/1977022/200210414.pdf
    Raj_2018          : https://aip.scitation.org/doi/10.1063/1.5011433
    Oklopcic_2016     : https://iopscience.iop.org/article/10.3847/0004-637X/832/1/30.
    Newport_fiber     : https://www.newport.com/t/fiber-optic-basics
    Prunty_2014       : https://iopscience.iop.org/article/10.1088/0031-8949/89/12/128001
    Rudder_1968       : https://doi.org/10.1364/JOSA.58.001260
    Meulenbroeks_1992 : https://doi.org/10.1103/PhysRevLett.69.1379
    Rowell_1971       : https://doi.org/10.1063/1.1675125
    Milenko_1971      : https://doi.org/10.1016/0031-8914(72)90223-6
    Feng_2021         : https://doi.org/10.1063/5.0027481
    Kolos_2004        : https://doi.org/10.1063/1.1840870
    Long_2002         : https://doi.org/10.1002/0470845767
    Mohlmann_1985     : https://doi.org/10.1016/0009-2614(85)80685-0
"""
import pathlib
import scipy.constants as c
import numpy           as np
import pandas          as pd
from scipy.interpolate       import interp1d

# Database constants for various particles
###############################################################################
# Mass
m       = {'e-' :  c.m_e           ,# kg
           'p+' :  c.m_p           ,# kg
           'H'  :  1.00784e-3/c.N_A,# kg
           'H2' :  2.01588e-3/c.N_A,# kg
           'H2+':  2.01588e-3/c.N_A,# kg
           'D2' :  4.02820e-3/c.N_A,# kg
           'D2+':  4.02820e-3/c.N_A,# kg
           'He' :  4.00260e-3/c.N_A,# kg
           'N2' : 28.01340e-3/c.N_A,# kg
           'O2' : 31.99880e-3/c.N_A,# kg
           'Ar' : 39.94800e-3/c.N_A,# kg
           'Ar+': 39.94855e-3/c.N_A,# kg
           'CO2': 44.00950e-3/c.N_A}# kg
# charge state
q       = {'e-' : -1,               # 
           'p+' : +1,               #
           'H'  :  0,               # 
           'H2' :  0,               # 
           'H2+': +1,               #
           'D2' :  0,               # 
           'D2+': +1,               #            
           'He' :  0,               #
           'N2' :  0,               #
           'O2' :  0,               #
           'Ar' :  0,               #
           'Ar+': +1,               #
           'CO2':  0}               #
# Rotationnal constants
B       = {'e-' : np.nan         ,  #
           'p+' : np.nan         ,  #
           'H'  : np.nan         ,  #
           'H2' :6085.300*c.h*c.c,  # J       % \ref{NIST_CCC}
           'H2+': np.nan         ,  # J       % some argue (\ref{Long_2002}) there are no anisotropy (i.e. no Raman scattering), but \ref{Mohlmann_1985} were saying that he saw strong spectral sho from Raman scattering on H2+
           'D2' :3044.360*c.h*c.c,  # J       % \ref{NIST_CCC}
           'D2+': np.nan         ,  # J       % some argue (\ref{Long_2002}) there are no anisotropy (i.e. no Raman scattering), but \ref{Mohlmann_1985} were saying that he saw strong spectral sho from Raman scattering on H2=
           'He' : np.nan         ,  #
           'N2' : 199.824*c.h*c.c,  # J       % \ref{NIST_CCC}
           'O2' : 143.768*c.h*c.c,  # J       % \ref{NIST_CCC}
           'Ar' : np.nan         ,  #
           'Ar+': np.nan         ,  #
           'CO2':  39.021*c.h*c.c}  # J       % \ref{NIST_CCC}

# Degenerency factor of the state
gJ_ev   = {'e-' : np.nan,           #
           'p+' : np.nan,           #
           'H'  : np.nan,           #
           'H2' : 1     ,           #         % \ref{Oklopcic_2016} and  \ref{Long_2002} 
           'H2+': np.nan,           #         % value to find with ref if ro-vibrationnal transitions
           'D2' : 6     ,           #         % \ref{Long_2002} 
           'D2+': np.nan,           #         % value to find with ref  if ro-vibrationnal transitions
           'He' : np.nan,           #
           'N2' : 6     ,           #         % \ref{Penney_1974} and  \ref{Long_2002} 
           'O2' : 0     ,           #         % \ref{Penney_1974} and  \ref{Long_2002} 
           'Ar' : np.nan,           #
           'Ar+': np.nan,           #
           'CO2': 1     }           #         % \ref{Penney_1974}
gJ_od   = {'e-' : np.nan,           #
           'p+' : np.nan,           #
           'H'  : np.nan,           #
           'H2' : 3     ,           #         % \ref{Oklopcic_2016} and  \ref{Long_2002} 
           'H2+': np.nan,           #         % value to find with ref  if ro-vibrationnal transitions
           'D2' : 3     ,           #         % \ref{Long_2002} 
           'D2+': np.nan,           #         % value to find with ref  if ro-vibrationnal transitions
           'He' : np.nan,           #
           'N2' : 3     ,           #         % \ref{Penney_1974} and  \ref{Long_2002} 
           'O2' : 1     ,           #         % \ref{Penney_1974} and  \ref{Long_2002} 
           'Ar' : np.nan,           #
           'Ar+': np.nan,           #
           'CO2': 0     }           #         % \ref{Penney_1974}
# Nuclear spin
I       = {'e-' : np.nan,           #
           'p+' : np.nan,           #
           'H'  : np.nan,           #
           'H2' : 1/2   ,           #         % \ref{Long_2002} 
           'H2+': 1/2   ,           #
           'D2' : 1     ,           #         % \ref{Long_2002} 
           'D2+': 1     ,           #  
           'He' : np.nan,           #
           'N2' : 1     ,           #         % \ref{Long_2002} 
           'O2' : 0     ,           #         % \ref{Long_2002} 
           'Ar' : np.nan,           #
           'Ar+': np.nan,           #
           'CO2': 0     }
# Depolarization ratio for Rayleigh scattering almost zero for simple molecules (like Thgomson scattering Rayleigh scattering is usally mostly along the same polarization as the laser polarisation)
rho_Ray = {'e-' : np.nan   ,        #
           'p+' : np.nan   ,        #
           'H'  : np.nan   ,        #
           'H2' : 0.339e-2 ,        #         % \ref{Rudder_1968}  NB: 2.3e-3 from \ref{Meulenbroeks_1992} at 300K and 532nm
           'H2+': np.nan   ,        #         % value to find with ref
           'D2' : np.nan   ,        #         % value to find with ref
           'D2+': np.nan   ,        #         % value to find with ref
           'He' : 0.0045e-4,        #         % \ref{Rudder_1968}
           'N2' : 0.590e-2 ,        #         % \ref{Rudder_1968}  NB: 2.69e-3 from \ref{Meulenbroeks_1992} at 300K and 532nm
           'O2' : 0.78e-2  ,        #         % \ref{Rowell_1971}
           'Ar' : 0.127e-4 ,        #         % \ref{Rudder_1968}
           'Ar+': 0.127e-4 ,        #         % \ref{Rudder_1968} supposed to be the same as Ar?
           'CO2': 1.01e-3  ,        #         % \ref{Rowell_1971}
           'Xe' : 0.519e-4 ,        #         % \ref{Rudder_1968}
           'CH4': 0.225e-4 }        #         % \ref{Rudder_1968}
# Depolarization ratio for Raman sattering (under Placzeks polarizability approximation (correct for simple light bi-atomic molecules))
rho_Ram = {'e-' : np.nan,           #
           'p+' : np.nan,           #
           'H'  : np.nan,           #
           'H2' : 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'H2+': 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'D2' : 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'D2+': 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'He' : np.nan,           #
           'N2' : 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'O2' : 3/4   ,           #         % typicall value for simple molecules, it may be worth to find a detailed ref
           'Ar' : np.nan,           #
           'Ar+': np.nan,           #
           'CO2': 3/4   }     
# Anisotropy of the molecular-polarisability tensor @ 532nm and its uncertainty
gamma   = {'e-' : 0               , # F m^2   % not possible to polarize an electron 
           'p+' : 0               , # F m^2   % almost not possible to polarize a proton
           'H'  : np.nan          , # F m^2   % spherical atom
           'H2' : 2.78444365e-42  , # F m^2   % \ref{Raj_2018} @532nm, 2.0169839837558403e-42Fm^2 if taken from \ref{Kolos_2004} or \ref{Feng_2021}
           'H2+': np.nan          , # F m^2   % value to find with ref if ro-vibrationnal transitions
           'D2' : 2.69141893e-42  , # F m^2   % \ref{Raj_2018} @532nm, 2.56682904693072e-42Fm^2 if taken from \ref{Kolos_2004} or \ref{Feng_2021}
           'D2+': np.nan          , # F m^2   % value to find with ref if ro-vibrationnal transitions
           'He' : 0               , # F m^2   % spherical atom
           'N2' : 0.62849025e-41  , # F m^2   % 8% uncertainty \ref{Penney_1974}  + \ref(vandeSande_2002)
           'O2' : 1.00995049e-41  , # F m^2   % 10% uncertainty \ref{Penney_1974} + \ref(vandeSande_2002)
           'Ar' : 0               , # F m^2   % spherical atom
           'Ar+': 0               , # F m^2   % spherical atom
           'CO2': 2.01990099e-41  } # F m^2   % 13% uncertainty \ref{Penney_1974} + \ref(vandeSande_2002)
dgamma  = {'e-' : 0               , # F m^2   % not possible to polarize an electron 
           'p+' : 0               , # F m^2   % almost not possible to polarize a proton
           'H'  : np.nan          , # F m^2   % spherical atom
           'H2' : 0               , # F m^2   % uncertainty not known \ref{Raj_2018} @532nm, 2.0169839837558403e-42Fm^2 if taken from \ref{Kolos_2004} or \ref{Feng_2021}
           'H2+': np.nan          , # F m^2   % value to find with ref if ro-vibrationnal transitions
           'D2' : 0               , # F m^2   % uncertainty not known  \ref{Raj_2018} @532nm, 2.56682904693072e-42Fm^2 if taken from \ref{Kolos_2004} or \ref{Feng_2021}
           'D2+': np.nan          , # F m^2   % value to find with ref if ro-vibrationnal transitions
           'He' : 0               , # F m^2   % spherical atom
           'N2' : gamma['N2']*8e-2, # F m^2   % 8% uncertainty \ref{Penney_1974}  + \ref(vandeSande_2002)
           'O2' : gamma['N2']*1e-1, # F m^2   % 10% uncertainty \ref{Penney_1974} + \ref(vandeSande_2002)
           'Ar' : 0               , # F m^2   % spherical atom
           'Ar+': 0               , # F m^2   % spherical atom
           'CO2': gamma['N2']*13e-2}# F m^2   % 13% uncertainty \ref{Penney_1974} + \ref(vandeSande_2002)
# Microscopic polarisability @ 532nm for differential Rayleigh cross-section estimation
alpha   = {'e-' : 0               , # F m^2   % not possible to polarize an electron 
           'p+' : 0               , # F m^2   % almost not possible to polarize a proton 
           'H'  :  5.903972e-42   , # F m^2   % \ref{Long_2002}, \ref{NIST_CCC-BDB} gives a value very close 
           'H2' :  7.305786e-42   , # F m^2   % \ref{Raj_2018} @ 532nm, 6.989495859424321e-42Fm^2 if taken from \ref{Milenko_1971} or \ref{Feng_2021}
           'H2+': np.nan          , # F m^2   % value to find with ref
           'D2' :  7.210006e-42   , # F m^2   % \ref{Raj_2018} @ 532nm, 7.042620986301121e-42Fm^2 if taken from \ref{Milenko_1971} or \ref{Feng_2021}
           'D2+': np.nan          , # F m^2   % value to find with ref
           'He' :  2.352782e-41   , # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
           'N2' : 19.652439e-41   , # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
           'O2' : 17.818551e-41   , # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
           'Ar' : 18.536127e-41   , # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
           'Ar+': 11.614220e-41   , # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
           'CO2': 29.416539e-41   } # F m^2   % \ref(vandeSande_2002) with formula 2.35 giving alpha= np.sqrt(dSigmadOmega)*laser['lamda']**2*c.epsilon_0/c.pi
# Maximal number of of Raman lines to simulate (Raman simulations can be time consuming)
N_l     = {'e-' : 0     ,           #
           'p+' : 0     ,           #
           'H'  : 0     ,           #
           'H2' : 100   ,           #
           'H2+': 0     ,           #
           'D2' : 100   ,           #
           'D2+': 0     ,           #
           'He' : 100   ,           #
           'N2' : 100   ,           #
           'O2' : 100   ,           #
           'Ar' : 0     ,           #
           'Ar+': 0     ,           #
           'CO2': 100   }    
# Regroupe databases together to simplify things
dtbs    = {'m'      : m      ,   
           'q'      : q      ,  
           'B'      : B      ,
           'gJ_ev'  : gJ_ev  ,
           'gJ_od'  : gJ_od  ,
           'I'      : I      ,
           'rho_Ram': rho_Ram,
           'rho_Ray': rho_Ray,
           'gamma'  : gamma  ,
           'dgamma' : dgamma ,
           'alpha'  : alpha  ,
           'N_l'    : N_l
           }

# Transmission efficiency of various optics
###############################################################################
path       = pathlib.Path(__file__).parent                                     # get absolute path for data calibration files (same path as this .py file)
trans      = {'W broad': 2*c.pi*c.c/np.linspace(450e-9,1100e-9,10000)}
trans_data = ['transmittance_VBG-NF_532nm',
              'diffraction-efficiency_1300lmm-IBSEN-grating',
              'quantum-efficiency_iCCD-GenIII-HBf',
              'quantum-efficiency_iCCD-GenIII-HRf',
              'quantum-efficiency_iCCD-GenII-SR',
              'transmittance_14m-WF-Optran-fiber',
              'typical-insertion-losses-fiber',
              'typical-transmittance-poly-filters',
              'quantum-efficiency_Si-APD-excelitas']
trans_names= ['VBG-NF','diff grating','quant eff ICCD HBf',                    # transmission coeff of transmission grating based Thomson diagnostic
              'quant eff ICCD HRf','quant eff ICCD SR',
              'fiber','insertion fiber','filters poly', 'quant eff Si APD']    # transmission coeff of polychromator based Thomson diagnostic



for i,elt in enumerate(trans_names):
    trans[elt]           = pd.read_csv(str(path)+'/'+trans_data[i]+'.dat',sep='\t')
    trans[elt,'func']    = interp1d(2e9*c.pi*c.c/trans[elt].values[:,0],
                                    trans[elt].values[:,1],
                                    kind='quadratic',bounds_error=False,fill_value=0)
    
trans['trans grating','func'] = lambda W: trans['VBG-NF','func'](W)          *\
                                          trans['diff grating','func'](W)    *\
                                          trans['quant eff ICCD HBf','func'](W)
  
trans['polychromator','func'] = lambda W: trans['fiber','func'](W)           *\
                                          trans['insertion fiber','func'](W) *\
                                          trans['filters poly','func'](W)    *\
                                          trans['quant eff Si APD','func'](W)   