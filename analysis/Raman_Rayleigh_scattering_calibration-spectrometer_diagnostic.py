# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 17:01:45 2021

@author: Benjamin Vincent

The script is used to extract the calibration coefficients from a Rayleigh or 
Raman calibration
"""
import sys
import time
import json
import pathlib
import datetime
import numpy               as np
import matplotlib.pyplot   as plt
import pandas              as pd
import scipy.constants     as c
# import astropy.convolution as ap

sys.path.append('../') # add root of the project to the default paths

# from brokenaxes                      import brokenaxes
from scipy.optimize                 import curve_fit
from theory.Rayleigh                import Rayleigh_spectrum
from theory.Raman                   import Raman_spectrum
from data_fixed.get_fixed_data      import dtbs, trans#, trans_names

t_start = time.time()

# Initalize the dictionnaries and give them a detailed name
VBG     = {'name'       : 'VBG',
           'description': 'caracteristics of VBG filter used to filter out strayligh from reflection and Rayleigh scattering'}
detector= {'name'       : 'detector',
           'description': 'caracteristics of the detector used to measure the scattering signal (end of the detection branch)'}
collect = {'name'       : 'collect',
           'description': 'caracteristics of collection optics (start ofd the detection branch)'}
spectro = {'name'       : 'spectro',
           'description': 'caracteristics of the spectrometer used to disperse the light collected'}
laser   = {'name'       : 'laser',
           'description': 'caracteristics of the laser used to generate the scattering signal'}
geom    = {'name'       : 'geom',
           'description': 'geometry of the scattering configuration'}
calib   = {'name'       : 'calib',
           'description': 'outputs from the Rayleigh or Raman scattering calibration'}

np.seterr(divide='ignore', invalid='ignore')                                   # ignore the warning messsage from matplotlib in the function used to switch between rad/s and nm

###############################################################################
###############################################################################
############################### INITIALIZATIONS ###############################
###############################################################################
###############################################################################

###############################################################################
# Defintion of the method used for calibration and sources files              #
###############################################################################
calib['method']               = 'Raman'                                        # Raman, Rayleigh, of both method are available (maybe one could add Rayleigh for various pressure? or one could apply the same algorithm at multiple pressure and then compare the calibration coefficient with another script ... I guess this is better)
calib['instrument file name'] = '2021-7-6  Instrumental function 1bin.csv'     # Name of the file used for the instrument function calibration (laser wavelength and spectral resoution are extracted from this)
calib['scattering file name'] = '2021-7-6  Raman calibration 8bins 10p4mbar.csv'# Name if the file used for the absolute calibration of the diagnostic (laser wavelength and detection branches loss coefficient are extracted from this)

###############################################################################
# Defintion and calculation of variables related to the transmission branch   #
###############################################################################

# Paramters of the laser used to generated the scattering signal usefull for the calibration procedure
laser['energy']  = .4                                                # J       % laser energy
laser['lambda']  = 532e-9                                            # m       % laser wavelength
laser['dlambda'] = .5e-9                                             # m       % possible uncertainty on the laser wavelength
laser['omega']   = 2*c.pi*c.c/laser['lambda']                        # rad s^-1% laser frequency
laser['domega']  = 2*c.pi*c.c*laser['dlambda']/laser['lambda']**2    # rad s^-1% possible uncertainty on the laser frequency

###############################################################################
# Defintion and calculation of variables related to the detection branch      #
###############################################################################

# Paramters of the detector (iCCD or PMT usually) usefull for the calibration procedure
detector['height']        = 13e-3                                        # m       % full detector array height of the sensor (CCD or PMT array) often equal to the fiber bundle length if light is transmitted to the spectrometer with a fiber 
detector['V bin']         = 1024                                         #         % number of pixel bin along the vertical direction (along laser beam direction)
detector['N pix']         = 1024                                         #         % number of pixel over one direction of the iCCD (hypothesis of square iCCD)
detector['counts per ph'] = 2#3.87*454                                   #         % number of e- accumulated on the CCD per photon electron create on the photocathode by the incoming photons (with a given quantum efficiency). This value depend on the type of detector, the MCP gains and the digitalization gain and speed

# Paramters of the Volume Bragg Grating filter
VBG['lambda']             = 532e-9                                       #         % Full Width at Half Maximum of the filtering interval of the laser
VBG['dlambda']            = .15e-9                                       #         % Full Width at Half Maximum of the filtering interval of the laser
VBG['omega']              = 2*c.pi*c.c/VBG['lambda']                     # rad s^-1% laser frequency
VBG['domega']             = 2*c.pi*c.c*VBG['dlambda']/VBG['lambda']**2   # rad s^-1% possible uncertainty on the laser frequencyVBG['dtheta']             = np.radians(1.2)                              # rad     % acceptance angle of the volume Bragg grating
VBG['width']              = 50e-3                                        # m       % width of the VGB notch filter (usually a square)

# Paramters of the collection lens configuration usefull for the calibration procedure
collect['OA']             = 520e-3                                       # m       % distance of the collection lens from the laser beam    (value from Yanis for the AWAKE setup experiment)
collect['phi']            = 100e-3                                       # m       % diameter of the collection lens (value from Yanis for the AWAKE setup experiment)
collect['f']              = 200e-3                                       # m       % focal of the collection lens  (value from Yanis for the AWAKE setup experiment)
collect['OAp']            = abs(collect['f']*collect['OA']/(collect['f']-collect['OA']))# m       % image distance knowing the collection focal length and the image-lens distance (to be miniumized to maximize the solid angle of detection)
collect['gamma']          = collect['OAp']/collect['OA']                 #         % image magnification of the collection lens
collect['dOmega']         = 2*c.pi*(1-np.cos(np.arctan(.5*collect['phi']/collect['OA'])))# steradian       % solid angle of collection
collect['L']              = detector['height']/collect['gamma']          # m       % length of the obervation volume
collect['d']              = 150e-6                                       # m       % diameter of the laser beam at the observation volume position
collect['losses name']    = ['VBG-NF','quant eff ICCD HBf']              # m       % name of the losses coefficient taken into consideration before the estimation of the remaining losses estimation obtained after the Raman fit

# known transmission coefficients of the detection branch (the Raman fit give the transmission coefficient due to the remaining losses that were not yet correctly characterized)
trans['losses','func']    = lambda W: np.prod([trans[name,'func'](W) for name in collect['losses name']],axis=0)

###############################################################################
# Defintion of variables related to the scattering geometry                   #
###############################################################################

# Scattering configuration
geom['Ki']   = np.array([1, 0, 0])                         #         % direction of the wavevector of the incident wave
geom['Ks']   = np.array([0, 1, 0])                         #         % direction of the wavevector of the scattered wave
geom['Ei0']  = np.array([0, 0, 1])                         #         % direction of the electric field of the incident wave
geom['Ki']   = geom['Ki'] /np.linalg.norm(geom['Ki'])      #         % to be sure i is normed
geom['Ks']   = geom['Ks'] /np.linalg.norm(geom['Ks'])      #         % to be sure i is normed 
geom['Ei0']  = geom['Ei0']/np.linalg.norm(geom['Ei0'])     #         % to be sure i is normed
geom['K']    = geom['Ks']-geom['Ki']                       #         % scattering wavevector 
geom['Kn']   = geom['K'] /np.linalg.norm(geom['K'])        #         % normed scattering wavevector 
geom['theta']= np.arccos(np.dot(geom['Ki'],geom['Ks']))    # rad     % scattering angle          
geom['phi']  = np.arccos(np.dot(geom['Ei0'],np.cross(geom['Ki'],geom['Ks'])))# rad     % angle between the normal to the scattering plane and the polarisation vector of the incident wave              
geom['psi']  = 0                                           # rad     % angle between polarisation of the incident wave and the one accepted by the detection branch. Assign this variable to a char ('both' for example) to estimate the contribution from both polarizations

print('''
#######################🔎 Light collection 🔎##################################
* Simulation with a {D_c:g} mm in diameter and a {f:g} focal length collection lens
positionned {OA:g} mm from the laser beam image the scattering volume with a
{gamma:g} magnification
* With this configuration, the diagnostic probe a {L_probe:g} mm volume length, 
photons are collected over {domega:g} sr
* The hypothesis that the laser beam diameter fit the slit width or fiber 
diameter is done'''
.format(D_c     = collect['phi']*1e3, OA     = collect['OA']*1e3, gamma = collect['gamma'] , 
        L_probe = collect['L']*1e3  , domega = collect['dOmega'], f     = collect['f']))
     
print('''
######################📐 Scattering geometry 📐################################
* Scattering angle of {theta:g} °
* Angle between scattering plane and EM wave polarisation is {phi:g} °'''
.format(theta = np.degrees(geom['theta']), phi = np.degrees(geom['phi'])), end='')
if isinstance(geom['psi'],(int,float)):
    print('''
* Only scattered light making an angle of {psi:g} ° with the EM wave polarisation 
is transmitted through the detection branch'''.format(psi = np.degrees(geom['psi'])))
else:
    print('''
* Both scattered light polarizations directions, are transmitted through the 
detection branch''')

###############################################################################
# Particles properties definition for Raman and Rayleigh scattering           #
###############################################################################
P0 = 1040                              # Pa      % ambient pressure of the medium probed
T0 = 293                               # K       % ambient temperature of the medium probed

scat = {'name' : 'N2'                , #         % Name of scattering particle
        'P'    : P0                  , # Pa      % partial pressure of the scattering particle
        'T'    : T0                  , # K       % Temperature of scattering particle
        'n'    : P0/(c.k*T0)         , # m^-3    % Density of of scattering particle
        'n_rov': 'unknown before fit', # m^-3    %  Ro-vibrationnal states density of of scattering particle
        'v'    : 0                   , # m s^-1  % Drift velocity of scattering particle
        'dist' : 'Maxwellian'          #         % Type of scattering particle behavior for velocity distribution consideration => nothing implemented fo neutrals
        }

spectrums = {'Rayleigh': Rayleigh_spectrum, # dictionnary with the theoritical spectrum functions used for the scattering calibrations fits
             'Raman'   : Raman_spectrum  }

###############################################################################
###############################################################################
################################## LOAD DATA ##################################
###############################################################################
###############################################################################

instrument             = pd.read_csv('../data_input/'+calib['instrument file name'],usecols = (0, 1))
instrument['omega']    = 2*c.pi*c.c/instrument['wavelength']
instrument['domega']   = abs(instrument['omega'].diff())

scattering             = pd.read_csv('../data_input/'+calib['scattering file name'],usecols = (0, 1))
scattering['omega']    = 2*c.pi*c.c/scattering['wavelength']
scattering['domega']   = abs(scattering['omega'].diff())
scattering['domega'][0]= scattering['domega'][1]
scattering['photon']   = scattering['intensity']/(detector['counts per ph']*trans['losses','func'](scattering['omega']))
scattering['dEdW']     = scattering['photon']*c.hbar*scattering['omega']/scattering['domega']

###############################################################################
###############################################################################
############################ CALIBRATION FITTINGS #############################
###############################################################################
###############################################################################

## fitting the instrumental function => spectral resolution + one estimation of laser wavelength
###############################################################################

# prepare the function used for instrument profile fitting + the first guess and some bounds parameters for fit
profile = lambda W, mu, sigma, A , offset: offset+A*np.exp(-.5*((W-mu)/sigma)**2) 
intens0 = np.max(instrument['intensity'])
domega0 = np.mean(instrument['domega']) 
offset0 = np.mean(instrument['intensity'])
norm0   = offset0

# prerform the fit with the curve_fit function from scipy
p_opt, p_cov = curve_fit(lambda W, mu, sigma, A , offset: profile(W,mu*laser['omega'], sigma*domega0, A*intens0, offset)/norm0,
                          instrument['omega'], instrument['intensity']/norm0        ,
                          p0     =  [1                               , 10  , 1   ,  0      ] ,
                          bounds = ([1-laser['domega']/laser['omega'], 1e-2, 1e-3, -abs(offset0/10)], 
                                    [1+laser['domega']/laser['omega'], 1e2 , 1e3 ,  abs(offset0/10)]))

# store the information related to the calibration
calib['instrument fit']       = profile(instrument['omega'], p_opt[0]*laser['omega'], p_opt[1]*domega0, p_opt[2]*intens0, p_opt[3]).tolist()
calib['omega_i instrument']   = p_opt[0]*laser['omega']
calib['omega_res instrument'] = p_opt[1]*domega0
calib['offset instrument']    = p_opt[3]


## Fitting the Rayleigh or Raman scattering spectrum  => scattering gas temperature + another estimation of the laser wavelength (from Doppler shift of an artificial v velocity) + transmission coefficient of the detection branch (from unknown sources)
###############################################################################

# prepare the function used for scattering spectrum fitting + norm coefficient to ease convergence + some initial guess parameter values
if calib['method']=='both':
    dEdW = lambda W, v, tau: (spectrums['Raman']   (W, 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], v, '',sigma_inst=calib['omega_res instrument']) +\
                              spectrums['Rayleigh'](W, 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], v, '',sigma_inst=calib['omega_res instrument']))*\
                             tau*detector['V bin']/detector['N pix']
else:
    dEdW = lambda W, v, tau: spectrums[calib['method']](W, 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], v, '',sigma_inst=calib['omega_res instrument'])*\
                             tau*detector['V bin']/detector['N pix']                                

# restrict the interval depending of the type of calibration perfomed
if calib['method']=='Raman':                                                   # If Raman calibration is perfomed, then we should exclude the central interval where there is straylight from Rayleigh and reflections
    interval = abs(scattering['omega']-calib['omega_i instrument'])>1.5*np.max([calib['omega_res instrument'],VBG['domega']])
else:                                                                          # Otherwise Rayleigh scattering contribution is included for the calibration and no restriction on the fitting interval is required (but one has to be sure that the Rayleigh contribution is much higher than reflections a the scattering particle density used for the calibration)
    interval = scattering['omega']>0                                           # Just check that frequencies are positive in the data
calib['omegas rejected'] = scattering['omega'].loc[interval==False].to_list()

# prepare normalisation coefficient to ease curve fit algorithm
v0     = c.c*(laser['omega']-calib['omega_i instrument'])/(2*laser['omega']*np.sin(geom['theta']/2))# m s^-1  % typical order of magnitude expected for the electron drift velocity
trans0 = 1e-2                                                                  # excpected transmission coefficient of the detection branch due to supplemetary losses from sources not caracterized                                    
norm0  = np.mean(scattering['dEdW'].loc[interval==True])                       # norm the value on which the leat-square should be done in order ease the fitting procedure

# prerform the fit with the curve_fit function from scipy
p_opt, p_cov = curve_fit(lambda  W, v, trans: dEdW(W,  v*v0 , trans*trans0)/norm0 ,
                          scattering['omega'].loc[interval==True]   , scattering['dEdW'].loc[interval==True]/norm0,
                          p0     =  [           1, 1   ], 
                          bounds = ([-c.c/abs(v0), 1e-5], 
                                    [ c.c/abs(v0), 1e5]))

# store the information related to the calibration
calib['scattering fit']       = dEdW(scattering['omega'] ,p_opt[0]*v0, p_opt[1]*trans0).tolist()
calib['omega_i scattering']   = laser['omega']-p_opt[0]*v0*2*laser['omega']*np.sin(geom['theta']/2)/c.c# the velocity of the scattering particle is used as a fitting parameter to have flexibility on the exact wavelength of the incident laser light
calib['trans scattering']     = p_opt[1]*trans0

# call function used for scattering spectrum fitting to print info
if calib['method']=='both':
    spectrums['Raman']   (scattering['omega'], 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], p_opt[0], 'talk',sigma_inst=calib['omega_res instrument'])
    spectrums['Rayleigh'](scattering['omega'], 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], p_opt[0], 'talk',sigma_inst=calib['omega_res instrument'])
else:
    spectrums[calib['method']](scattering['omega'], 1, laser, geom, collect, dtbs, scat, scat['n'], scat['T'], p_opt[0], 'talk',sigma_inst=calib['omega_res instrument'])

###############################################################################
###############################################################################
#################################### PLOTS ####################################
###############################################################################
###############################################################################

## Scattering configuration
###############################################################################
fig = plt.figure(2)
rand_x = np.random.uniform(-1, 1, size=(20,)) # just some random x coordinates to plot plot the magnetic field like a stream
rand_y = np.random.uniform(-1, 1, size=(20,)) # just some random y coordinates to plot plot the magnetic field like a stream
rand_z = np.random.uniform(-1, 1, size=(20,)) # just some random z coordinates to plot plot the magnetic field like a stream
# B_0n   = geom['B0']/2.5
ax = fig.add_subplot(projection='3d')
ax.quiver(-1.5         , 0            , 0            , 3             , 0             , 0             ,
          arrow_length_ratio=0   , color='C2', linewidth=.5)
ax.quiver(0            , 0            , 0            , geom['Ki'][0] , geom['Ki'][1] , geom['Ki'][2] ,
          arrow_length_ratio=0.25, color='C2', label='$\\vec{k}_{i}$')
ax.quiver(0            , 0            , 0            , geom['Ks'][0] , geom['Ks'][1] , geom['Ks'][2] ,
          arrow_length_ratio=0.25, color='C1', label='$\\vec{k}_{s}$')
ax.quiver(geom['Ki'][0], geom['Ki'][1], geom['Ks'][2], geom['K'][0]  , geom['K'][1]  , geom['K'][2]  ,
          arrow_length_ratio=0.25, color='C3', label='$\\vec{k}$')
ax.quiver(0            , 0            , 0            , geom['Ei0'][0], geom['Ei0'][1], geom['Ei0'][2],
          arrow_length_ratio=0.25, color='C0', label='$\\vec{E}_{i0}$')
# for i in range(20):
#     ax.quiver(rand_x[i], rand_y[i]    , rand_z[i]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
#               arrow_length_ratio=0.5, color='C4')
# ax.quiver(rand_x[0]    , rand_y[0]    , rand_z[0]    , B_0n[0]       , B_0n[1]       , B_0n[2]       ,
#           arrow_length_ratio=0.5, color='C4',label='$\\vec{B}_{0}$')
ax.grid(False) 
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
# ax.axis(False)
ax.set_xlim(-1.5, 1.5)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1) 
ax.legend()
plt.title('Scattering configuration \n $\\theta = {:g}$ ° and $\\phi = {:g}$ °'.format(np.degrees(geom['theta']) , np.degrees(geom['phi'])) )
# plt.savefig('Scattering_configuration_theta = %1.3f deg_phi = %1.3f deg.pdf'% (np.degrees(geom['theta']) , np.degrees(geom['phi'])))
plt.show()

## fit of the instrument function shape
###############################################################################
fig, ax = plt.subplots()
ax.axvline(calib['omega_i instrument'],
           linestyle=':' , marker='' , label='$\omega_{i}$ instrument', color='C9')
ax.plot   (instrument['omega'],instrument['intensity']            ,
           linestyle=''  , marker='+', label='instrument function data')
ax.plot   (instrument['omega'],calib['instrument fit'],
           linestyle='--', marker='' , label='Gaussian fit')
ax.legend()
ax.set_xlim(calib['omega_i instrument']-5*calib['omega_res instrument'], 
            calib['omega_i instrument']+5*calib['omega_res instrument'])
ax.set_xlabel('pulsation /($rad.s^{-1}$)')
ax.set_ylabel('intensity /(counts)')
secax = ax.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
plt.show()


## fit of the #photon Rayleigh or Raman scattered over the spectral channels or CCD macropixels
###############################################################################
fig, ax = plt.subplots()
ax.axvline(calib['omega_i scattering'],
            linestyle='--', marker='' , label='$\omega_{i}$ from scattering', color='C8')
ax.axvline(calib['omega_i instrument'],
            linestyle=':' , marker='' , label='$\omega_{i}$ instrument'     , color='C9')
ax.plot   (scattering['omega'], calib['scattering fit'],
            linestyle='-' , marker='' , label=calib['method']+' fit')
ax.plot   (scattering['omega'].loc[interval==True], scattering['dEdW'].loc[interval==True],
            linestyle='--', marker='+', label=calib['method']+' data')
ax.plot   (scattering['omega'].loc[interval==False], scattering['dEdW'].loc[interval==False],
            linestyle='--', marker='+', label='data excluded')
ax.legend()
ax.set_xlim(scattering['omega'].iloc[0], scattering['omega'].iloc[-1])
ax.set_ylim(0                          , 1.5*np.max(calib['scattering fit']))
ax.set_xlabel('pulsation /($rad.s^{-1}$)')
ax.set_ylabel('intensity /(counts)')
secax = ax.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
secax.set_xlabel('wavelength /($nm$)')
plt.show()

###############################################################################
###############################################################################
############################ SAVE CALIBRATION DATA ############################
###############################################################################
###############################################################################
geom  = {key: geom[key] for key in geom if key not in ['Ki', 'Ks','Ei0','K','Kn']} # delete some geom entries not really necessary (and that would create error as theyhave not been converted to lists)

# Make list of the dictionnaries to save in the json file. NB: it is better if all dictionnary have a name entry
liste = [calib,laser,detector,VBG,collect,scat,geom]

# Prepare the name of the calibration data file depending on the date of the input data file used for the scattering calibration
in_fname  = pathlib.Path('../data_input/'+calib['scattering file name'])
ftime     = datetime.datetime.fromtimestamp(in_fname.stat().st_ctime)
out_fname = str(ftime.year)+'-'+str(ftime.month)+'-'+str(ftime.day)+'_'+calib['method']+'-scattering-calibration'

# save in a json file
with open('../data_output/'+out_fname+'.json', 'w') as fname:
    json.dump(liste, fname, indent=4)