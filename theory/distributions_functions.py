# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 17:05:03 2021

@author: Benjamin Vincent
"""

import scipy.constants   as c
import numpy             as np

from scipy.special       import erfc, gamma 

###############################################################################
# Distribution functions for Thomson spectrum or 1D-EVDF (no hypothesis of particles without drift velocity)
###############################################################################

def Gaussian(X , mu, sigma):
    """
    Give the Gaussian distribtion function around the mean value `mu` with a
    standard deviation `sigma`
    
    Parameters
    ----------
    X     : `np.ndarray`
        array with the x values
    mu    : float
        central value of the distribution
    sigma : float
        standard deviation of the distribution
    
    Returns
    -------
    f : `np.ndarray`
        Array of the distribution function values for the various values of X
        
    """
    f = np.exp(-.5*((X-mu)/sigma)**2)/np.sqrt(2*c.pi*sigma**2)
    
    return f


def ERF(X , mu, sigma):
    """
    Give the ERF distribtion function (that is obtain from the derivation of
    the Druyvesteyn distribution) around the mean value `mu` with a standard 
    deviation `sigma`
    
    Parameters
    ----------
    X     : `np.ndarray`
        array with the x values
    mu    : float
        central value of the distribution
    sigma : float
        standard deviation of the distribution
    
    Returns
    -------
    f : `np.ndarray`
        Array of the distribution function values for the various values of X
    """
    
    alpha = gamma(5/4)/(3*gamma(3/4))
    beta  = .5*np.sqrt(c.pi*gamma(5/4)/(3*gamma(3/4)**3)) 
    
    f = beta*erfc(alpha*((X-mu)/sigma)**2)/sigma
    
    return f

###############################################################################
# EVDF Distribution functions (under the hypothesis of particles without drift velocity)
###############################################################################

def Maxwell(X, sigma):
    """
    Give the Maxwell distribtion function with a standard deviation `sigma`
    
    Parameters
    ----------
    X : `np.ndarray`
        array with the x values
    sigma: float
        standard deviation of the distribution
    
    Returns
    -------
    f : `np.ndarray`
        Array of the distribution function values for the various values of X
    """
    
    f = np.sqrt(2/c.pi)*X**2*np.exp(-.5(X/sigma)**2)/sigma**3
    
    return f

def Druyvesteyn(X , sigma):
    """
    Give the Druyvesteyn distribtion function with a standard deviation `sigma`
    
    Parameters
    ----------
    X : `np.ndarray`
        array with the x values
    sigma: float
        standard deviation of the distribution
            
    Returns
    -------
    f : `np.ndarray`
        Array of the distribution function values for the various values of X
    """
    
    alpha = gamma(5/4)/(3*gamma(3/4))
    beta  = .5*np.sqrt(c.pi*gamma(5/4)/(3*gamma(3/4)**3)) 
    
    f = 8*alpha*beta*X**2*np.exp(-alpha*2*(X/sigma)**4)/(sigma**3*np.sqrt(c.pi))
    
    return f

