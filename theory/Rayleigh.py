# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 10:07:59 2020

@author: Benjamin Vincent

NB: the depolarisation ratio for perpendicular Rayleigh scattering being
usually weak for the small molecules (that we will consider), we neglect the 
contribution from the depolarized scattering cross-section
"""

import scipy.constants       as c
import numpy                 as np

from theory.distributions_functions import Gaussian

def Rayleigh_spectrum(W       , n_os,
                      laser   , geom, collect, dtbs, part,
                      n       , T   , v      , 
                      _print  ,
                      **kwargs):
    """
    Give the Rayleigh spectrum function for a given scattering configuration 
    and depending on the scattering particles properties
    
    
    Parameters
    ----------
    W    : `np.ndarray`
        Pulsation array over which the spectrum will be evaluated
        
    n_os : float
        over sampling used over the detector real pulsation resolution in order 
        to have theoritcal curves with a resolution high enough to not miss the
        the sharp Raman, Rayleigh, and Thomson cyclotroic peaks 
        
    laser['omega']  : float
        Pulsation of the incident EM wave use to generate Rayleigh scattering
        
    laser['energy'] : float
        Laser energy used to generate the scattering signal (usually the pulse
        energy of a Q-switch laser)        
        
    geom['theta'] : float
        Scattering angle
        
    geom['phi']   : float
        Polarization angle
        
    collect['L']      : float
        Length of the observation volume
         
    collect['dOmega'] : float
        Solid angle over which the scattered light is collected
        
    dtbs['m']     : dict 
        Dictionnary with the mass of scattering species 
    
    dtbs['alpha'] : dict
        Dictionnary with the microscopic polarisability from which we get the
        differential Rayleigh scattering cross section         
        
    part['name'] : str
        Name of the particle species generating Rayleigh scattering
        
    n     : float 
        Temperature of the scattering species (equal to part['n'] for 
        simulations). It should be defined as a separated parameter to use the 
        same function for fittings.
        
    T     : float 
        Density of the scattering species (equal to part['T'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.

    v     : float 
        Velocity of the scattering species (equal to part['v'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.        
 
    _print : str
        print option to show text
       
        
    Optionnal parameters (**kwargs)
    ----------        
    pressure      : float 
        partial pressure of the scattering species 
        
    T_rov      : float (equal T if no value is given) 
        Ro-vibrationnal temperature of the scattering species (equal to 
        part['T_rov'] for simulations). It should be defined as a separated 
        parameter to use the same function for fittings.    
    
    sigma_inst : float (equal 0 if no value is given) 
        spectral resolution from the instrument function if one does not want 
        to perform the convolution after        
       
        
    Returns
    -------
    dE_s : `np.ndarray`
        Array with the distribution of the scattered energy as a function of W
        
    """
    
# Check optionnal parameters and define their default values
###############################################################################    
    T_rov   = T
    sigma_I = 0
    
    if 'T_rov' in kwargs:
        T_rov      = kwargs['T_rov']
    if 'sigma_inst' in kwargs:
        sigma_I = kwargs['sigma_inst']
    if 'pressure' in kwargs:
        n = kwargs['pressure']/(c.k*T)
        
# Check if their are entries inside the database for this scattering particle
###############################################################################        
    if not part['name'] in dtbs['alpha']:
        print('''
###############################💥 Rayleigh 💥##################################
No values related to Rayleigh scattering for this particle in the database
''')
        Rayleigh_spectrum.dN_s = 0
        return W*0    
    
    err        = 0                                                             # Init the error message to off (i.e. 0)
    dtb = {}                                                                   # init the database dictionnary for the scattering species
    for keys in dtbs:                                                          # Get all dtbs entries related to the scattering particle
        if part['name'] in dtbs[keys]: 
            dtb[keys]=dtbs[keys][part['name']]
           
# Cross section
###############################################################################

# mean polarisability contribution, related to the 'alpha' coefficient (usually dominant)
    dsigma_a  = (c.pi*dtb['alpha']/c.epsilon_0)**2*(laser['omega']/(2*c.pi*c.c))**4 \
                *(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)            # differential Rayleigh cross section at the laser w_i and from polarisability of the electron cloud around of the atom/molecule
    
# ro-vibrationnal contribution due to J->J transition, related to the 'gamma' coefficient (usually negligible)
    if ~np.isnan(dtb['B']):                                                             # If B value, then it is a molecules and there is a ro-vibrationnal contribution to the Rayleigh scattering
        J_ev = np.array([2*i   for i in range(dtb['N_l'])])
        J_od = np.array([2*i+1 for i in range(dtb['N_l'])]) 
        
        Q    = c.Boltzmann*T_rov*(2*dtb['I']+1)**2/(2*dtb['B'])                # rotational partition function  
                  
        nJ_od = n*dtb['gJ_od']*(2*J_od+1)                                     \
                *np.exp(-dtb['B']*J_od*(J_od+1)/(c.Boltzmann*T_rov))/Q         # density of molecules i the J odd rotationnal state
        nJ_ev = n*dtb['gJ_ev']*(2*J_ev+1)                                     \
                *np.exp(-dtb['B']*J_ev*(J_ev+1)/(c.Boltzmann*T_rov))/Q         # density of molecules i the J even rotationnal state
                 
        b_JJ_od = J_od*(J_od+1)/((2*J_od-1)*(2*J_od+3))                        # Placzek-Teller coefficients odd J
        b_JJ_ev = J_ev*(J_ev+1)/((2*J_ev-1)*(2*J_ev+3))                        # Placzek-Teller coefficients even J
   
        dSigmdOmega_od  = (nJ_od/n)*(4*b_JJ_od/45)*(dtb['gamma']*laser['omega']**2/(c.epsilon_0*c.c**2))**2
        dSigmdOmega_ev  = (nJ_ev/n)*(4*b_JJ_ev/45)*(dtb['gamma']*laser['omega']**2/(c.epsilon_0*c.c**2))**2

        if isinstance(geom['psi'],(int,float)):                                # NB: by default we make the hypothesis that we systematically look at one polarization direction of the scattered radiation (otherwise we sum both polarizations contributions see 'else' case)
            dsigma_g = (np.sum(dSigmdOmega_od)+np.sum(dSigmdOmega_ev))          \
                       *((1-dtb['rho_Ram'])*(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)*np.cos(geom['psi'])**2+dtb['rho_Ram'])# differential Rayleigh cross section at the laser w_i and from anisotropy of the electron cloud around of the atom/molecule
        else:                                                                  # if geom['psi'] is a char, then it means we look a both polarisations, and we have to sum both contributions
            dsigma_g = (np.sum(dSigmdOmega_od)+np.sum(dSigmdOmega_ev))          \
                       *((1-dtb['rho_Ram'])*(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)+2*dtb['rho_Ram'])# differential Rayleigh cross section at the laser w_i and from anisotropy of the electron cloud around of the atom/molecule
    else:
        dsigma_g = 0
    
    dsigma = dsigma_a+dsigma_g
    
# Spectral form factor
###############################################################################
    k       = np.sqrt(laser['omega']**2+W**2-2*laser['omega']*W*np.cos(geom['theta']))/c.c# probed wavector (without the assumption v/c<<1 but with the asumption that theta does not change much over the collection solid angle)
    sigma_D = np.sqrt(c.k*T/dtb['m'])*k                                        # spectrum broadening due to Doppler effect 
    sigma   = np.sqrt(sigma_D**2+sigma_I**2)                                   # + broadening from instrumental function if a value is given in the optionnal parameters
    mean    = laser['omega']-k*v
    Sk      = Gaussian(W , mean, sigma)                                        # Spectral form factor due to Doppler Broadening   
    
    if np.mean(sigma)<3*abs(W[1]-W[2]):                                        # np.mean(sigma) because sigma depends on k that depend on the spectral shift... But for usuall spectral shift it is almost constant
        err=1
    
# Spectral distribution of the energy scattered
###############################################################################
    dE_s    = n*collect['L']*laser['energy']*dsigma*Sk*collect['dOmega']
    dN_s    = n*collect['L']*laser['energy']*dsigma*collect['dOmega']/(c.hbar*laser['omega'])
    Rayleigh_spectrum.dN_s = dN_s
    
    if _print=='talk':
        print('''
###############################💥 Rayleigh 💥##################################
* {species}
* sigma = {sig:g} m^2 over dOmega = {dOm:g} sr
* n = {n:g} m^-3 , T = {T:g} K, and v = {v:g} m.s^-1
* photons scattered \u2248 {NPhotons:g} in total over dOmega'''
        .format(species=part['name'], sig=dsigma, dOm=collect['dOmega'],n=n,T=T,v=v,NPhotons=dN_s),end='')
        if ~np.isnan(dtb['B']):
            print('''
* the scattering particle is a molecule with possible J->J ro-vibrationnal 
transition that contribute up to {sigRatio:g}% to the Rayleigh scattering signal.
If significant, this contribution can lead to a depolarisation ratio lower 
than 1.'''
                .format(sigRatio=100*dsigma_g/dsigma),end='')
            if isinstance(geom['psi'],(int,float)):
                print('''
* for J->J transitions, we look only at one polarization direction tilted 
by {psi:g} ° relative to the polarization direction of the incident wave'''
                    .format(psi=np.degrees(geom['psi'])),end='')
            else:
                print('''
* for J->J transitions, we look at both polarizations direction, a summation 
has been performed''',end='')    
        if err==1:
            print('''
🔔 WARNING: The natural width of the Raman lines are probably too sharp 
({sigma_lines:g} rad.s^-1) to be resolved even with the n_os coefficient of {n_os:g}
that lead to a resolution for calculation of {sigma_os:g} rad.s^-1.'''
                .format(sigma_lines=np.mean(sigma),n_os=n_os, sigma_os=abs(W[1]-W[2])),end='')
        print('''
###############################################################################''')
    
    return dE_s