# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 10:07:59 2020

@author: Benjamin
"""
import scipy.constants       as c
import numpy                 as np
import matplotlib.pyplot     as plt

from theory.distributions_functions import Gaussian

def Raman_spectrum(W       , n_os,
                   laser   , geom, collect, dtbs, part,
                   n       , T   , v   , 
                   _print  ,
                   **kwargs):
    """
    Give the Raman spectrum function for a given scattering configuration 
    and depending on the scattering particles properties
    
    
    Parameters
    ----------
    W    : `np.ndarray`
        Pulsation array over which the spectrum will be evaluated
        
    n_os : float
        over sampling used over the detector real pulsation resolution in order 
        to have theoritcal curves with a resolution high enough to not miss the
        the sharp Raman, Rayleigh, and Thomson cyclotroic peaks 
        
    laser['omega']  : float
        Pulsation of the incident EM wave use to generate Rayleigh scattering
        
    laser['energy'] : float
        Laser energy used to generate the scattering signal (usually the pulse
        energy of a Q-switch laser)
        
    geom['theta'] : float
        Scattering angle
        
    geom['phi']   : float
        Polarization angle
            
    geom['ksi']   : float
        Angle between the incident laser beam polarization (supposed to be 
    polarized at 100%) and the polarization direction selected by the detection 
    branch   
        
    collect['L']      : float
        Length of the observation volume
        
    collect['dOmega'] : float
        Solid angle over which the scattered light is collected
        
    part['name']  : str
        Name of the particle species generating Rayleigh scattering
        
    dtb['m']       : dict 
        Dictionnary with the mass of scattering species 
        
    dtb['gamma']   : dict
        Dictionnary with
        
    dtb['N_lines'] : float 
        Values of the number of Raman lines to calulate (starting from the ones
        closer to the laser wavelength)
        
    dtb['B']       : dict
        Dictionnary with the value of the rotationnal constant
        
    dtb['gJ_ev']   : dict
        Dictionnary with the value of the weighted factor due to the 
        degenerency of even rotationnal states 
        
    dtb['gJ_od']   : dict
        Dictionnary with the value of the weighted factor due to the 
        degenerency of odd rotationnal states
        
    dtb['I']       : dict
        Dictionnary with the value of the nuclear spin number
        
    dtb['rho']     : float
        Value of the depolarization ratio of the molecule’s Raman band
        
    n     : float 
        Temperature of the scattering species (equal to part['n'] for 
        simulations). It should be defined as a separated parameter to use the 
        same function for fittings.
        
    T     : float 
        Density of the scattering species (equal to part['T'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.
        
    v     : float 
        Velocity of the scattering species (equal to part['v'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.       
        
    _print : str
        print option to show text
        
        
    Optionnal parameters (**kwargs)
    ----------        
    pressure      : float 
        partial pressure of the scattering species
    
    T_rov      : float (equal T if no value is given) 
        Ro-vibrationnal temperature of the scattering species (equal to 
        part['T_rov'] for simulations). It should be defined as a separated 
        parameter to use the same function for fittings.    
    
    sigma_inst : float (equal 0 if no value is given) 
        spectral resolution from the instrument function if one does not want 
        to perform the convolution after
    
    
    Returns
    -------
    dE_s : `np.ndarray`
        Array with the distribution of the scattered energy as a function of W
        
    """
    
# Check optionnal parameters and define their default values
###############################################################################    
    T_rov   = T
    sigma_I = 0
    
    if 'T_rov' in kwargs:
        T_rov      = kwargs['T_rov']
    if 'sigma_inst' in kwargs:
        sigma_I = kwargs['sigma_inst']
    if 'pressure' in kwargs:
        n = kwargs['pressure']/(c.k*T)
        
# Check if their are entries inside the database for this scattering particle
###############################################################################        
    if not part['name'] in dtbs['B']:
        print('''
################################💥 Raman 💥####################################
No values related to Raman scattering for this particle in the database
''')
        Raman_spectrum.dN_s = 0
        return W*0
    
    err        = 0                                                             # Init the error message to off (i.e. 0)
    dtb = {}                                                                   # init the database dictionnary for the scattering species
    for keys in dtbs:                                                          # Get all dtbs entries related to the scattering particle
        if part['name'] in dtbs[keys]: 
            dtb[keys]=dtbs[keys][part['name']]

    dtb['N_l'] = np.min([dtb['N_l']                                       ,    # reduce the number of Raman line in case the rotationnal energy becomes higher than the energy of the laser photons
                         int(np.floor(laser['omega']*c.hbar/(8*dtb['B'])))])
    
# Rotationnal quantum numbers                                                 
###############################################################################
    J_ev = np.array([2*i   for i in range(dtb['N_l'])])
    J_od = np.array([2*i+1 for i in range(dtb['N_l'])])
    
# Longueur d'onde des pics ram
###############################################################################
    W_stokes_od   = laser['omega']-dtb['B']*(4*J_od+6)/c.hbar
    W_stokes_ev   = laser['omega']-dtb['B']*(4*J_ev+6)/c.hbar
    W_astokes_od  = laser['omega']+dtb['B']*(4*J_od-2)/c.hbar
    W_astokes_ev  = laser['omega']+dtb['B']*(4*J_ev-2)/c.hbar
    
    W_peaks       = np.concatenate([W_stokes_od, W_stokes_ev, W_astokes_od, W_astokes_ev])
    
# Density of excited states
###############################################################################
    Q = c.k*T_rov*(2*dtb['I']+1)**2/(2*dtb['B'])                               # rotational partition function
    
    nJ_stokes_od  = n*dtb['gJ_od']*(2*J_od+1)*np.exp(-dtb['B']*J_od*(J_od+1)/(c.k*T_rov))/Q
    nJ_stokes_ev  = n*dtb['gJ_ev']*(2*J_ev+1)*np.exp(-dtb['B']*J_ev*(J_ev+1)/(c.k*T_rov))/Q
    nJ_astokes_od = n*dtb['gJ_od']*(2*J_od+1)*np.exp(-dtb['B']*J_od*(J_od+1)/(c.k*T_rov))/Q
    nJ_astokes_ev = n*dtb['gJ_ev']*(2*J_ev+1)*np.exp(-dtb['B']*J_ev*(J_ev+1)/(c.k*T_rov))/Q
    
    nJ            = np.concatenate([nJ_stokes_od, nJ_stokes_ev, nJ_astokes_od, nJ_astokes_ev])

# Placzek-Teller coefficients
###############################################################################
    b_JJp2_od = 3*(J_od+1)*(J_od+2)/(2*(2*J_od+1)*(2*J_od+3))
    b_JJp2_ev = 3*(J_ev+1)*(J_ev+2)/(2*(2*J_ev+1)*(2*J_ev+3))
    b_JJm2_od = 3* J_od   *(J_od-1)/(2*(2*J_od+1)*(2*J_od-1))
    b_JJm2_ev = 3* J_ev   *(J_ev-1)/(2*(2*J_ev+1)*(2*J_ev-1))
    
# Cross section of the transitions
###############################################################################
    dSigmdOmega_stokes_od  = (4*b_JJp2_od/45)*(dtb['gamma']*W_stokes_od**2 /(c.epsilon_0*c.c**2))**2
    dSigmdOmega_stokes_ev  = (4*b_JJp2_ev/45)*(dtb['gamma']*W_stokes_ev**2 /(c.epsilon_0*c.c**2))**2
    dSigmdOmega_astokes_od = (4*b_JJm2_od/45)*(dtb['gamma']*W_astokes_od**2/(c.epsilon_0*c.c**2))**2
    dSigmdOmega_astokes_ev = (4*b_JJm2_ev/45)*(dtb['gamma']*W_astokes_ev**2/(c.epsilon_0*c.c**2))**2
    
    if isinstance(geom['psi'],(int,float)):                                    # NB: by default we make the hypothesis that we systematically look at one polarization direction of the scattered radiation (otherwise we sum both polarizations contributions see 'else' case)
        dSigmdOmega        = np.concatenate([dSigmdOmega_stokes_od, dSigmdOmega_stokes_ev, dSigmdOmega_astokes_od, dSigmdOmega_astokes_ev])\
                             *((1-dtb['rho_Ram'])*(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)*np.cos(geom['psi'])**2+dtb['rho_Ram']) 
    else:                                                                      # if geom['psi'] is a char, then it means we look a both polarisations, and we have to sum both contributions
        dSigmdOmega        = np.concatenate([dSigmdOmega_stokes_od, dSigmdOmega_stokes_ev, dSigmdOmega_astokes_od, dSigmdOmega_astokes_ev])\
                             *((1-dtb['rho_Ram'])*(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)/2+dtb['rho_Ram']) 
                             
# Intensity of the transitions                                            
###############################################################################
    P_peaks = nJ*dSigmdOmega

# Spectral form factor
###############################################################################
    k       = np.sqrt(laser['omega']**2+W**2-2*laser['omega']*W*np.cos(geom['theta']))/c.c                   
    sigma_D = np.sqrt(c.k*T/dtb['m'])*k                                        # spectrum broadening due to Doppler effect 
    sigma   = np.sqrt(sigma_D**2+sigma_I**2)                                   # + broadening from instrumental function if a value is given in the optionnal parameters
    
    Sk  = lambda W_p: Gaussian(W , W_p-k*v, sigma)                             # Spectral form factor due to Doppler Broadening 
 
    if np.mean(sigma)<3*abs(W[1]-W[2]):                                        # np.mean(sigma) because sigma depends on k that depend on the spectral shift... But for usuall spectral shift it is almost constant
        err=1

# Raman spectra output                                                    
###############################################################################
    dE_s = collect['L']*laser['energy']*np.sum([P_peaks[i]*Sk(W_peaks[i]) for i in range(4*dtb['N_l'])],axis=0)*collect['dOmega']
    dN_s = collect['L']*laser['energy']*np.sum( P_peaks/(c.hbar*W_peaks))*collect['dOmega']
    Raman_spectrum.dN_s = dN_s
    
    if _print=='talk':
        print('''
################################💥 Raman 💥####################################
* {species}
* sigma = {sig:g} m^2 over dOmega = {dOm:g} sr
* n = {n:g} m^-3 , T = {T:g} K, and v = {v:g} m.s^-1
* photons scattered \u2248 {NPhotons:g} in total over dOmega'''
        .format(species=part['name'], sig=np.mean(dSigmdOmega), dOm=collect['dOmega'],n=np.sum(nJ)/2,T=T,v=v,NPhotons=dN_s),end='') # NB: n=np.sum(nJ)/2 because the array with the nj density contains twice the real neutral density as it is the same ro-vibrationnal states that contribute to the stokes and anti-stokes Raman lines
        if isinstance(geom['psi'],(int,float)):
            print('''
* we look only at one polarization direction tilted by {psi:g} ° relative to the 
polarization direction of the incident wave'''
                .format(psi=np.degrees(geom['psi'])),end='')
        else:
            print('''
* we look at both polarizations direction, a summation has been performed''',end='')                  
        if err==1:
            print('''
🔔 WARNING: The natural width of the Raman lines are probably too sharp 
({sigma_lines:g} rad.s^-1) to be resolved even with the n_os coefficient of {n_os:g}
that lead to a resolution for calculation of {sigma_os:g} rad.s^-1.'''
                .format(sigma_lines=np.mean(sigma),n_os=n_os, sigma_os=abs(W[1]-W[2])),end='')
        print('''
###############################################################################''')    

    if 0:
        ## typical energy per Raman peak
        ###############################################################################
        
        # in case on want to tune parameters directly here
        collect['L']      = 1e-2
        collect['dOmega'] = .1
        laser['energy']   = 1
        
        # plot
        fig, ax = plt.subplots()
        ax.axvline( laser['omega'],
                    linestyle='--', marker='' , label='$\omega_{i}$ laser', color='C8')
        ax.plot   (W_peaks,  collect['L']*laser['energy']*P_peaks*collect['dOmega'],
                    linestyle='none' , marker='o' , label='energy per Raman peak')
        ax.legend()
        ax.set_xlim(2*c.pi*c.c/540e-9, 2*c.pi*c.c/525e-9)
        ax.set_xlabel('pulsation /($rad.s^{-1}$)')
        ax.set_ylabel('energy /(J)')
        secax = ax.secondary_xaxis('top', functions=(lambda x: 2e9*c.pi*c.c/x, lambda x: 2e9*c.pi*c.c/x))
        secax.set_xlabel('wavelength /($nm$)')
        plt.title(part['name']+' at '+str(part['P'])+' Pa, with '+str(laser['energy'])+' J, '+str(collect['dOmega'])+' sr, and '+str(collect['L'])+' m')
        plt.savefig('Raman_peaks_energy.png', dpi=300, facecolor='w', edgecolor='w',
                    orientation='landscape',bbox_inches='tight')
        plt.show()

    return dE_s