# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 10:07:59 2020


@author: Benjamin Vincent

List of references:
    BOB_xxxx : http://xxx
"""

import scipy.constants       as c
import numpy                 as np

from scipy.special                  import kn, ive
from theory.distributions_functions import Gaussian, ERF

def Thomson_spectrum(W     , n_os,
                     laser , geom, collect, dtbs, part, 
                     n     , T   , v      , 
                     _print,
                     **kwargs): 
    """
    Give the Thomson spectrum function for a given scattering configuration 
    and depending on the scattering particles properties
    
    Parameters
    ----------
    W    : `np.ndarray`
        Pulsation array over which the spectrum will be evaluated
        
    n_os : float
        over sampling used over the detector real pulsation resolution in order 
        to have theoritcal curves with a resolution high enough to not miss the
        the sharp Raman, Rayleigh, and Thomson cyclotroic peaks 
        
    laser['omega']  : float
        Pulsation of the incident EM wave use to generate Rayleigh scattering
       
    laser['energy'] : float
        Laser energy used to generate the scattering signal (usually the pulse
        energy of a Q-switch laser)       
        
    geom['phi']     : float
        Polarization angle
        
    geom['beta']    : float
        Angle between the magnetic field and the probed wavecector
       
    geom['theta_s'] : float
        Angle between the magnetic field and the scattering wavecector
        
    geom['Bm']      : float
        Magnitude of the magnetic field  

    geom['theta']   : float
        Scattering angle
        
    collect['dtheta'] : float
        Fluctuations of the scattering angle over the collection lens area  (approximately equal to the fluctuation with the magnetic field angle for a small NA collection lens configuration)      
        
    collect['L']      : float
        Length of the observation volume
        
    collect['d']      : float
        diameter of the observation volume    
        
    collect['dOmega'] : float
        Solid angle over which the scattered light is collected

    dtb['m']     : dict 
        Dictionnary with the mass of scattering species 
        
    dtb['q']    : float 
        Charge of the scattering species
                
    part['name'] : str
        Name of the particle species generating Thomson scattering
        
    part['dist'] : str 
        Thomson spectrum options depending on the scattering particles
        properties. The following entries are currently available :
            - 'Maxwellian': for scattering particle at thermal equilibriu. In 
            this case, the Thomson spectrum is a Gaussian.
            - 'Druyvesteyn': for scattering particle out of thermal equilibrium
            due to highly energetic electrons population damped by ionization
            processes for example. In this case the Thomson spectrum is a ERF
            function.
            - 'relativistic': for relativistic scattering particle (the impact 
            of magnetic field is ignored). In this case an approximation of the
            spectrum form factor is used. This formula is valid for Te from
            0eV to 100keV. (for forumla, see \ref{Prunty_2014})
            - 'magnetized' : for scattering particles that are strongly 
            magnetized around agnetic field lines
        
    n     : float 
        Temperature of the scattering species (equal to part['n'] for 
        simulations). It should be defined as a separated parameter to use the 
        same function for fittings.
        
    T     : float 
        Density of the scattering species (equal to part['T'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.

    v     : float 
        Velocity of the scattering species (equal to part['v'] for simulations). 
        It should be defined as a separated parameter to use the same function 
        for fittings.   
           
    _print : str
        print option to show text
     
        
    Optionnal parameters (**kwargs)
    ----------        
    sigma_inst : float (equal 0 if no value is given) 
        spectral resolution from the instrument function if one does not want 
        to perform the convolution after
     
        
    Returns
    -------
    dE_S : `np.ndarray`
        Array with the distribution of the scattered energy as a function of W
    """
# Check optionnal parameters and define their default values
###############################################################################    
    sigma_I = 0
    
    if 'sigma_inst' in kwargs:
        sigma_I = kwargs['sigma_inst']

# Check if their are entries inside the database for this scattering particle
###############################################################################         
    if not part['name'] in dtbs['q']: 
        print('''
###############################💥 Thomson 💥###################################
No values related to Thomson scattering for this particle in the database
''')
        Thomson_spectrum.dN_s = 0
        return W*0  
    
    err        = 0                                                             # Init the error message to off (i.e. 0)
    approx_msg = 0                                                             # Init the approx message to off (i.e. 0)
    dtb      = {}                                                              # Init the database dictionnary for the scattering species
    dtb['m'] = dtbs['m'][part['name']]
    dtb['q'] = dtbs['q'][part['name']]
    
    # Thomson scattering regime 
    ###########################################################################
    k       = np.sqrt(laser['omega']**2+W**2-2*laser['omega']*W*np.cos(geom['theta']))/c.c# Probed wavector (without the assumption v/c<<1 but with the asumption that theta does not change much over the collection solid angle)
    k_D     = 2*c.pi*np.sqrt(n*c.e**2/(c.epsilon_0*c.k*T))                     # Debye wave vector
    alpha_c = laser['lambda']*np.sqrt(n*c.e**2/(c.epsilon_0*c.k*T))/(4*c.pi*np.sin(geom['theta']/2))# Debye wave vector
    
    # Cross section
    ###########################################################################
    dsigma  = ((dtb['q']*c.e)**2/(4*c.pi*c.epsilon_0*dtb['m']*c.c**2))**2                 \
              *(1-(np.sin(geom['theta'])*np.sin(geom['phi']))**2)              # Differential Thomson cross section

    # Spectral form factor
    ###########################################################################
    sigma_D = np.sqrt(  c.k*T/dtb['m'])*k                                      # Spectrum broadening due to Doppler effect
    sigma_FT= np.sqrt(2*c.k*T/dtb['m'])*(2*c.pi/collect['d'])                  # Order of magnitude of the finite transit time broadening (most probable velocity used for the estimation)
    
    sigma   = np.sqrt(sigma_D**2+sigma_I**2)                                   # + broadening from instrumental function if a value is given in the optionnal parameters
    mean    = laser['omega']-k*v

    if part['dist']=='Maxwellian':
        Sk = Gaussian(W , mean, sigma)
        
    elif part['dist']=='Druyvesteyn':
        Sk = ERF(W , mean, sigma)
    
    elif part['dist']=='relativistic':                                         # \ref{Naito_1993}
        Wt    = W/laser['omega']
        alpha = dtb['m']*c.c**2/(2*c.k*T)
        u     = np.sin(geom['theta'])/(1-np.cos(geom['theta']))
        x     = np.sqrt(1+(Wt-1)**2/(2*Wt*(1-np.cos(geom['theta']))))
        y     = 1/np.sqrt(x**2+u**2)
        
        p0 =      4       +30*(x*y)**2              -55*(x*y)**4
        p1 =       -24*x*y             +545*(x*y)**3             -720*(x*y)**5
        p2 =  2*(33       -165*(x*y)**2             +240*(x*y)**4             -100*(x*y)**6)
        q0 = p0
        q1 = 25*(                       29*(x*y)**3              -42*(x*y)**5)
        q2 =  5*(18       -66*(x*y)**2             +630*(x*y)**4              -805*(x*y)**6)
        
        q  = 1-2*(x*y/alpha)*(p0+.5*p1/alpha+.25*p2/alpha**2)\
                            /(q0+.5*q1/alpha+.25*q2/alpha**2)
        
        if 2*alpha<600:
            S = ((.5/kn(2,2*alpha))       *(Wt**2/np.sqrt(1+Wt**2-2*Wt*np.cos(geom['theta'])))*
                 np.exp(-2*alpha*np.sqrt(1+(Wt-1)**2/(2*Wt*(1-np.cos(geom['theta'])))))
                /laser['omega'])
        else:                                                                  # For large alpha take the asymptotic behavior of the Bessel function to avoid undertemined division ('0/0')    
            approx_msg = 1                                                         
            S = (.5*np.sqrt(4*alpha/np.pi)*(Wt**2/np.sqrt(1+Wt**2-2*Wt*np.cos(geom['theta'])))*
                np.exp(-2*alpha*(np.sqrt(1+(Wt-1)**2/(2*Wt*(1-np.cos(geom['theta']))))-1))
                /laser['omega'])
        
        Sk = S*q
        
    elif part['dist']=='magnetized':                                           # \ref{Lehner_1969} NB: it is rather complicated to have experimental conditions with the cyclotronic peaks visisble, a first thing to make it easier is to force geom['beta']  = .99999*c.pi/2 
        wc = abs(c.e*dtb['q']*geom['Bm']/dtb['m'])                                # Scattering particle gyrofrequency
        a  = (k*np.sin(geom['beta'])/wc)**2*c.k*T/dtb['m']
        b  = 2*a/(np.tan(geom['beta'])**2)
        
        Ns = int(max(5*sigma)/wc)                                              # number of peak due to cyclotronic modulation over the Thomson spectrum (5*sigma)     
        Nr = int(abs(W[0]-W[-1])/wc)                                           # number of peak due to cyclotronic modulation over the spectral range probe by the spectrometer
        N  = min(Ns,Nr)                                                        # take the minimum of both numbers to avoid losing time with bessel functions estimations
        
        if geom['beta']>1e-3 and all(a<1e-3):                                  # it corresponds to the case with the scattering wavector almost perfectly parrallel to the magnetic field
            N   = 0                                                            # Only the central peak is significant, the Bessel function vanish at higher orders => no needs to estimate higher orders
            err = 1
        elif geom['beta']>1e-3 and all(a<1e-3):
            N   = 0                                                            # Only the central peak is significant, the Bessel function vanish at higher orders => no needs to estimate higher orders
            err = 2
        elif wc<3*n_os*abs(W[1]-W[2]):                                         # it correspond to the case when the cyclotronic peaks separation is so small that with the spectral resolution of the W array, we may miss some of the peaks
            err = 3
        
        if err==0 or err==1 or err==2:
            Sk  = W*0
            for N_peak in range(-N,N):
                Sk = Sk+np.exp(-((W-mean)/wc-N_peak)**2/b)*ive(N_peak,a)/np.sqrt(b*c.pi*wc**2)# \ref{first_Bessel} and \ref{Bessel_scale} 
        else:
            Sk = Gaussian(W , mean, sigma)
        
    # Spectral distribution of the energy scattered
    ###########################################################################
    dE_s    = n*collect['L']*laser['energy']*dsigma*Sk*collect['dOmega']
    dN_s    = n*collect['L']*laser['energy']*dsigma*collect['dOmega']/(c.hbar*laser['omega'])
    Thomson_spectrum.dN_s = dN_s
    
    if _print=='talk':
        print('''
###############################💥 Thomson 💥###################################
* {species} with {opt} behavior
* sigma = {sig:g} m^2 over dOmega = {dOm:g} sr
* n = {n:g} m^-3 , T = {T:g} K, and v = {v:g} m.s^-1
* scattering wave-vector \u2248 {k:g} m^-1, Debye wave-vector \u2248 {kD:g} m^-1
* coherence parameter \u2248 {alpha:g}
* broadenings: Doppler \u2248 {DopBroad:g} rad.s^-1, finite time \u2248 {FTBroad:g} rad.s^-1
* photons scattered \u2248 {NPhotons:g} in total over dOmega'''
                .format(species=part['name'], opt=part['dist'] , sig=dsigma   , dOm=collect['dOmega']  ,
                        n=n                 , T=T              , v=v          , k=np.mean(k)           ,
                        kD=k_D              , alpha=alpha_c    , NPhotons=dN_s, DopBroad=np.mean(sigma_D),
                        FTBroad=sigma_FT)   , end='')
        if part['dist']=='relativistic' and approx_msg==1:
            print('''
NB: For such a low temperature, the asymptote of the modified Bessel function 
of the second kind has to be used to avoid undertemined divisions.''',end='')

        elif part['dist']=='magnetized':
            if err==0:
                print('''
NB: Spectrum modulation might be visible due to magnetized electrons:
    - {Nspectrum:g} cyclotronic peaks over the spectrum range
    - {Nspectro:g} cyclotronic peaks over the spectrometer range
        => calculations done over {Nmin:g} peaks'''
                .format(Nspectrum=Ns ,Nspectro=Nr,Nmin=N),end='')
            elif err==1:
                print('''
🔔 WARNING: Due to a high magnetic field and/or low Te, only the central 
cyclotronic peak is significant, its width is the one expecxted for 
unmagnetized electrons shrinked by a the factor sin(beta) = {sinbeta:g}
same as the normal Maxwellian for unmagnetiyed cases.'''
                .format(sinbeta=np.sin(geom['beta'])),end='')
            elif err==2:
                print('''
🔔 WARNING: The scattering wave-vector direction seem to be perfectly aligned 
with B (beta = {betaa:g} deg), a high magnetic field and/or low Te can also 
explain these cases. For these extrem cases only the central cyclotronic peak 
is significant, its width is the same as the normal Maxwellian for unmagnetiyed 
cases.'''
                .format(betaa=geom['beta']*180/np.pi),end='')
            elif err==3:
                print('''
🔔 WARNING: The peaks due to cyclotronic modulation are probably too close to
each other ({domega_peak:g} rad.s^-1) in comparision to the pulsation resolution
({domega_res:g} rad.s^-1). For these extrem cases, the normal Maxwellian spectrum
is used.'''
                .format(domega_peak=wc, domega_res=abs(W[1]-W[2])/n_os),end='')
                
            if laser['omega']*np.sin(geom['theta_s'])*collect['dtheta']*np.sqrt(2*c.k*T/c.m_e)/c.c>wc:
                print('''
🔔 WARNING:  the fluctuations of the scattering angle over the collection lens
area might lead to the blurring of the cyclotronic peaks. By reducing the 
aperture of the collection lens, it might be possible to experimetally observe 
these peaks.'''
                ,end='')
                
            if 4*laser['omega']*np.sin(geom['theta']/2)*np.cos(geom['theta_s'])*2*c.k*T/(c.m_e*c.c**2)>wc:
                print('''
🔔 WARNING:  the fine structure of the cyclotronic peaks might be not be
visisble over the full spectrum'''
                ,end='')
                
        if k_D>np.mean(k): 
            print('''
🔔 WARNING: It seems that k_D>k, this wave-vector ordering indicate that there
may be collective features in the Thomson spectrum. For the while coherent 
scattering regime is not simulated.'''
            ,end='')                
        print('''
###############################################################################''')

    return dE_s